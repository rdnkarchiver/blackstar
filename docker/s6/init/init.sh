#!/command/with-contenv bash
# shellcheck shell=bash

set -euo pipefail

chown_blackstar() {
	find "$1" -not -user blackstar -not -group blackstar \
		-exec chown --no-dereference blackstar:blackstar -- {} +
}

BUID=${BUID:-1337}
BGID=${BGID:-1337}
TICK_SECONDS=${TICK_SECONDS:-3600}

usermod -o -u "$BUID" blackstar > /dev/null
groupmod -o -g "$BGID" blackstar > /dev/null

echo "
---
Using the following envionment variables:
BUID:          $BUID
BGID:          $BGID
TICK_SECONDS:  $TICK_SECONDS
---
"

mkdir -p /app/data/{db,logs,thumbnails}

if [[ ! -f '/app/data/blackstar.conf' ]]; then
	sed \
		-e 's|root = "/media"|root = "/app/media"|' \
		-e 's|database = "/var/blackstar/database"|database = "/app/data/db"|' \
		-e 's|thumbnail = "/var/blackstar/thumbnail"|thumbnail = "/app/data/thumbnails"|' \
		/app/src/configuration/blackstar.conf > /app/data/blackstar.conf
	echo "Generated configuration file '/app/data/blackstar.conf'."
	echo 'Please review this file, make any desired changes, and restart the container.'
	chown_blackstar /app/data
	exit 2
else
	chown_blackstar /app/data
fi
