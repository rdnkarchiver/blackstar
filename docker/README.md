# Run Blackstar in a Docker container

## Requirements
- [Docker](https://docs.docker.com/engine/install/)
- Nginx with the [Headers More](https://github.com/openresty/headers-more-nginx-module) module
- [Docker Compose](https://docs.docker.com/compose/install/) (recommended)


## Installation
Adjust volume paths to appropriate values.
Supported environment variables are in the table below.

### with Compose (recommended)
```yaml
services:
  blackstar:
    image: rdnk/blackstar:latest
    container_name: blackstar
    environment:
    - BUID=1000
    - BGID=1000
    - TICK_SECONDS=86400
    volumes:
    - /path/to/appdata:/app/data
    - /path/to/media:/app/media
    ports:
    - 7080:7080
    restart: unless-stopped
```

### without Compose
```
docker run --detach --name blackstar \
  --env 'BUID=1000' \
  --env 'BGID=1000' \
  --env 'TICK_SECONDS=86400' \
  --volume '/path/to/appdata:/app/data' \
  --volume '/path/to/media:/app/media' \
  --publish '7080:7080' \
  --restart unless-stopped \
  rdnk/blackstar:latest
```

## Details

### Environment variables
| Variable | Default | Description |
| --- | --- | --- |
| `BUID` | `1337` | The UID of a user with read access to the specified media directories. |
| `BGID` | `1337` | The GID of a user with read access to the specified media directories. |
| `TICK_SECONDS` | `3600` | The time between indexer runs. |

### Volumes

#### `/app/data`
This directory contains all relevant data used by the container (configuration, databases, logs, and thumbnails).
On first run, the container will generate a default configuration file and stop.
Once it generates, open it for editing, make any changes you want, and restart the container.

#### `/app/media`
This should be the root of your files you want in Blackstar.
It doesn't have to be `/app/media`, but if you use a different path, make sure you update the configuration file.

### Ports

#### `7080`
This is the port Blackstar listens on.
Blackstar leans heavily on a Nginx to serve content.
You should use the Nginx configuration [here](../configuration/nginx.conf) as a starting point.