import os
import typing
import pathlib
import sqlite3
import urllib.request
import contextlib

from .logging import LOGGER
from .configuration import Configuration

SCHEMA_PATH = os.path.realpath(os.path.join(os.path.dirname(__file__), 'schema'))

def _serialize_path(path: pathlib.PurePosixPath) -> bytes:
	return os.fsencode(os.fspath(path))

def _deserialize_path(data: bytes) -> pathlib.PurePosixPath:
	return pathlib.PurePosixPath(os.fsdecode(data))

sqlite3.register_adapter(pathlib.PurePosixPath, _serialize_path)
sqlite3.register_converter('PATH', _deserialize_path)

def format_uri(path: str, mode: str) -> str:
	return 'file:{}?mode={}'.format(urllib.request.pathname2url(path), mode)

def open_database(path: typing.Union[str, os.PathLike[str]], schema: str, writable: bool) -> sqlite3.Connection:
	create_database(path, schema)

	uri = format_uri(os.fspath(path), 'rw' if writable else 'ro')
	db = sqlite3.connect(uri, uri=True, detect_types=sqlite3.PARSE_DECLTYPES)

	try:
		db.row_factory = sqlite3.Row
		db.execute('PRAGMA synchronous = NORMAL')
		db.execute('PRAGMA foreign_keys = ON')
	except Exception:
		db.close()
		raise

	return db

def create_database(path: typing.Union[str, os.PathLike[str]], schema: str) -> None:
	try:
		fd = os.open(path, os.O_RDONLY|os.O_CREAT|os.O_EXCL|os.O_CLOEXEC, 0o660)
	except FileExistsError:
		return
	else:
		os.close(fd)

	LOGGER.info('Creating %s database at: %s', schema, path)

	uri = format_uri(os.fspath(path), 'rw')
	with contextlib.closing(sqlite3.connect(uri, uri=True)) as db:
		with open(os.path.join(SCHEMA_PATH, schema + '.sql'), 'r', encoding='utf8') as fp:
			db.executescript(fp.read())

def open_media_database(cfg: Configuration, writable: bool = False) -> sqlite3.Connection:
	return open_database(cfg.database.media, 'media', writable)

def open_reference_database(cfg: Configuration, writable: bool = False) -> sqlite3.Connection:
	return open_database(cfg.database.reference, 'reference', writable)
