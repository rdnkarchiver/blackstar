PRAGMA journal_mode = WAL;
PRAGMA synchronous = NORMAL;
PRAGMA user_version = 1;
PRAGMA foreign_keys = ON;

CREATE TABLE metadata (
	device INTEGER NOT NULL,
	inode INTEGER NOT NULL,
	type TEXT NOT NULL,
	size INTEGER NOT NULL,
	atime REAL NOT NULL,
	mtime REAL NOT NULL,
	ctime REAL NOT NULL,
	hash_md5 BLOB,
	hash_sha1 BLOB,
	hash_sha256 BLOB,
	width INTEGER,
	height INTEGER,
	duration REAL,
	vcodec TEXT,
	acodec TEXT,
	exif_make TEXT,
	exif_model TEXT,
	exif_software TEXT,
	exif_latitude REAL,
	exif_longitude REAL,
	perceptual INTEGER,
	thumbnail_1x TEXT,
	thumbnail_2x TEXT,
	thumbnail_3x TEXT,
	thumbnail_4x TEXT,
	preview_1x TEXT,
	preview_2x TEXT,
	count_link INTEGER NOT NULL DEFAULT 0,
	count_similar INTEGER NOT NULL DEFAULT 0,
	count_duplicate INTEGER NOT NULL DEFAULT 0,
	CHECK(type IN ('D', 'F', 'I', 'A', 'V')),
	PRIMARY KEY(device, inode)
) WITHOUT ROWID;

CREATE TABLE hierarchy (
	device_parent INTEGER NOT NULL,
	inode_parent INTEGER NOT NULL,
	device_child INTEGER NOT NULL,
	inode_child INTEGER NOT NULL,
	name PATH BLOB NOT NULL,
	FOREIGN KEY(device_parent, inode_parent) REFERENCES metadata(device, inode) ON UPDATE RESTRICT ON DELETE CASCADE,
	FOREIGN KEY(device_child, inode_child) REFERENCES metadata(device, inode) ON UPDATE RESTRICT ON DELETE CASCADE
);

CREATE TABLE similar (
	device_source INTEGER NOT NULL,
	inode_source INTEGER NOT NULL,
	device_target INTEGER NOT NULL,
	inode_target INTEGER NOT NULL,
	distance INTEGER NOT NULL,
	FOREIGN KEY(device_source, inode_source) REFERENCES metadata(device, inode) ON UPDATE RESTRICT ON DELETE CASCADE,
	FOREIGN KEY(device_target, inode_target) REFERENCES metadata(device, inode) ON UPDATE RESTRICT ON DELETE CASCADE
);

CREATE TABLE queue (
	device INTEGER NOT NULL,
	inode INTEGER NOT NULL,
	task TEXT NOT NULL,
	FOREIGN KEY(device, inode) REFERENCES metadata(device, inode) ON UPDATE RESTRICT ON DELETE CASCADE
);

CREATE UNIQUE INDEX metadata_pk ON metadata (device, inode);
CREATE INDEX metadata_sha256 ON metadata (hash_sha256);
CREATE INDEX metadata_thumbnail_1x ON metadata (thumbnail_1x);
CREATE INDEX metadata_thumbnail_2x ON metadata (thumbnail_2x);
CREATE INDEX metadata_thumbnail_3x ON metadata (thumbnail_3x);
CREATE INDEX metadata_thumbnail_4x ON metadata (thumbnail_4x);
CREATE INDEX metadata_preview_1x ON metadata (preview_1x);
CREATE INDEX metadata_preview_2x ON metadata (preview_2x);
CREATE INDEX hierarchy_parent ON hierarchy (device_parent, inode_parent);
CREATE INDEX hierarchy_child ON hierarchy (device_child, inode_child);
CREATE INDEX similar_src ON similar (device_source, inode_source);
CREATE UNIQUE INDEX queue_all ON queue (device, inode, task);
