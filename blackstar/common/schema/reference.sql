PRAGMA journal_mode = WAL;
PRAGMA synchronous = NORMAL;
PRAGMA user_version = 3;
PRAGMA foreign_keys = ON;

CREATE TABLE reference (
	id TEXT NOT NULL UNIQUE,
	expiration INTEGER,
	token TEXT,
	path PATH BLOB NOT NULL,
	download INTEGER NOT NULL DEFAULT 0,
	PRIMARY KEY("id")
);

CREATE UNIQUE INDEX reference_id ON reference (id);
CREATE INDEX reference_path ON reference (path);
CREATE INDEX reference_all ON reference (expiration, token, path, download);
