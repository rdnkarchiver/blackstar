import typing
import sqlite3
import pathlib

from .configuration import Configuration
from .node import Node

def get_paths(database: sqlite3.Connection, root: typing.Optional[Node], node: Node, sort: bool = True) -> list[pathlib.PurePosixPath]:
	result: list[pathlib.PurePosixPath] = []
	if root is None:
		return result

	for row in database.execute('SELECT device_parent, inode_parent, name FROM hierarchy WHERE device_child = ? AND inode_child = ?', (node.device, node.inode)):
		parent = Node(row['device_parent'], row['inode_parent'])
		name = row['name']

		if parent == root:
			result.append(name)
			continue

		parent_paths = get_paths(database, root, parent, False)
		for path in parent_paths:
			result.append(path / name)

	if sort:
		result.sort()

	return result
