import os
import sys
import types
import typing
import logging
import threading

LOGGER = logging.getLogger('blackstar.common')

MESSAGE_FORMAT_PROCESS = '[%(asctime)s] [%(process)7d] [%(levelname)8s] [%(name)24.24s] %(message)s'
MESSAGE_FORMAT_THREAD = '[%(asctime)s] [%(thread)7d] [%(levelname)8s] [%(name)24.24s] %(message)s'

DATE_FORMAT = '%Y-%m-%d %H:%M:%S'

def setup_logging(path: typing.Optional[os.PathLike[str]], level: int, fmt: str, datefmt: str, exception: bool) -> None:
	fspath: typing.Optional[str] = None if path is None else os.fspath(path)

	handler: logging.Handler
	if fspath and fspath != '-':
		handler = logging.FileHandler(fspath, errors=sys.stderr.errors)
	else:
		handler = logging.StreamHandler()

	handler.setFormatter(logging.Formatter(fmt, datefmt))
	logging.root.addHandler(handler)
	logging.root.setLevel(level)

	logging.captureWarnings(True)

	if '%(thread)' in fmt:
		logging.setLogRecordFactory(record_factory)

	if exception:
		sys.excepthook = exception_handler

def parse_log_level(level: str) -> int:
	mapping: dict[str, int] = getattr(logging, '_nameToLevel', {})
	try:
		return mapping[level.upper()]
	except KeyError:
		return int(level)

def record_factory(*args: typing.Any, **kwargs: typing.Any) -> logging.LogRecord:
	result = logging.LogRecord(*args, **kwargs)
	result.thread = threading.get_native_id()
	return result

def exception_handler(exc_type: typing.Type[BaseException], exc_value: BaseException, exc_traceback: typing.Optional[types.TracebackType]) -> None:
	if not issubclass(exc_type, Exception):
		return

	LOGGER.critical('Unhandled exception occured:', exc_info=(exc_type, exc_value, exc_traceback))
