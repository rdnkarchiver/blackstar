import os
import functools
import pathlib

from .configuration import Configuration
from .uniqueid import generate

ID_LENGTH = 12
EXTENSION = '.jpg'
EXTENSION_ANIMATED = '.mp4'
SPREAD = 2

generate_id = functools.partial(generate, ID_LENGTH)

def id_to_parts(id: str, animated: bool) -> pathlib.PurePosixPath:
	parts = [id[i] for i in range(0, SPREAD)] + [id + (EXTENSION_ANIMATED if animated else EXTENSION)]
	return pathlib.PurePosixPath(*parts)

def create_new(cfg: Configuration, animated: bool) -> tuple[str, int, pathlib.Path]:
	while True:
		id = generate_id()
		parts = id_to_parts(id, animated)
		path = cfg.directory.thumbnail / parts
		os.makedirs(path.parent, exist_ok=True)

		try:
			fd = os.open(path, os.O_WRONLY|os.O_CREAT|os.O_EXCL|os.O_CLOEXEC, 0o666)
		except FileExistsError:
			continue

		return id, fd, path
