import typing
import dataclasses
import traceback
import pathlib
import stat
import bottle

from ..common.reference import ID_LENGTH
from .configuration import get_current as get_configuration
from .token import Token
from .database import get_reference_db
from .reference import ReferenceCreator, ResolvedReference, get_or_create_reference_nolock, resolve_validate, update_environ, make_pure_path
from .application import master

@dataclasses.dataclass(frozen=True)
class Share:
	resolved: ResolvedReference
	expiration: int
	can_download: bool
	force_download: bool

@master.route('/action/share', name='share', method='POST', reference=True)
def share() -> typing.Optional[bottle.HTTPError]:
	if not bottle.request.token.can_share:
		return bottle.HTTPError(403, 'Share Restricted')

	share = get_share()
	if share.resolved.reference.token is not None and not share.resolved.reference.token.can_share:
		return bottle.HTTPError(403, 'Reference Share Restricted')

	update_environ('share', share.resolved)

	with get_reference_db():
		new_token = make_token(share, get_or_create_reference_nolock)
		new_reference = get_or_create_reference_nolock(new_token, make_pure_path(share.resolved.target), share.force_download)

	bottle.response.set_header('Cache-Control', 'private, no-store')
	bottle.redirect(bottle.request.app.get_url('dispatch', id=new_reference.id, pure=True), 201)
	return None

def get_share() -> Share:
	cfg = get_configuration()
	try:
		id = bottle.request.forms.getunicode('id', '')
		if (len(id) != ID_LENGTH):
			raise ValueError('invalid id length')

		expiration = int(bottle.request.forms.getunicode('expiration', cfg.authentication.expiration))
		if expiration < 0:
			raise ValueError('invalid expiration value')
		if expiration > cfg.authentication.expiration:
			expiration = cfg.authentication.expiration

		download = bottle.request.forms.getunicode('download', 'allow')
		if download == 'allow':
			can_download = True
			force_download = False
		elif download == 'force':
			can_download = True
			force_download = True
		elif download == 'restrict':
			can_download = False
			force_download = False
		else:
			raise ValueError('invalid download value')
	except Exception as e:
		raise bottle.HTTPError(400, 'Invalid Parameters', e, traceback.format_exc())

	resolved = resolve_validate(id)
	return Share(resolved, bottle.request.time + expiration, can_download, force_download)

def make_token(share: Share, creator: ReferenceCreator) -> Token:
	can_browse = False
	root = make_pure_path(share.resolved.target)

	if stat.S_ISDIR(share.resolved.stat.st_mode):
		can_browse = True
	elif stat.S_ISREG(share.resolved.stat.st_mode):
		root = root.parent

	root_reference = creator(None, root, False)

	return Token(
		expiration=share.expiration,
		root=root_reference.id,
		can_browse=(bottle.request.token.can_browse and can_browse) or None,
		can_download=(bottle.request.token.can_download and share.can_download) or None)
