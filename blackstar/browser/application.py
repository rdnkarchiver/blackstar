import os
import bottle

from .logging import ExceptionPlugin
from .configuration import ConfigurationPlugin
from .authorization import AuthorizationPlugin
from .database import DatabasePlugin

ROOT_PATH = os.path.realpath(os.path.dirname(__file__))
CONTENT_PATH = os.path.join(ROOT_PATH, 'content')
TEMPLATE_PATH = os.path.join(ROOT_PATH, 'template')

bottle.TEMPLATE_PATH.insert(0, TEMPLATE_PATH)

master = bottle.Bottle(autojson=False)
master.resources.base = ROOT_PATH
master.uninstall('template')
master.install(ExceptionPlugin())
master.install(ConfigurationPlugin())
master.install(AuthorizationPlugin())
master.install(DatabasePlugin())
