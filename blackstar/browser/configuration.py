import os
import time
import typing
import types
import functools
import traceback
import bottle
import gunicorn.config
import gunicorn.workers.base

from ..common.configuration import Configuration, load as _load
from ..common.device import DeviceMapper
from .logging import LOGGER, Logger

class ConfigurationPlugin:
	name = 'configuration'
	api = 2

	def __init__(self) -> None:
		self._cleanup: typing.Optional[typing.Callable[[], None]] = None

	def setup(self, app: bottle.Bottle) -> None:
		app.blackstar = types.SimpleNamespace()
		app.blackstar.configuration = None
		app.blackstar.device_mapper = DeviceMapper()
		app.blackstar.ensure_configuration = functools.partial(self._ensure, app)
		self._cleanup = functools.partial(delattr, app, 'blackstar')

	def close(self) -> None:
		if self._cleanup is not None:
			self._cleanup()
		self._cleanup = None

	def apply(self, callback: typing.Callable[..., typing.Any], route: bottle.Route) -> typing.Callable[..., typing.Any]:
		start = time.time()
		def wrapper(*args: typing.Any, **kwargs: typing.Any) -> typing.Any:
			if self._ensure(route.app):
				return callback(*args, **kwargs)
			else:
				LOGGER.error('Cannot process request (no configuration)')
				e: BaseException = route.app.blackstar.configuration
				return bottle.HTTPError(503, 'Configuration Not Loaded', e, ''.join(traceback.format_exception(type(e), e, e.__traceback__)))

		end = time.time()
		LOGGER.info('configuration plugin took %.5f seconds', end - start)
		return wrapper

	def _ensure(self, app: bottle.Bottle) -> bool:
		if app.blackstar.configuration is None:
			try:
				app.blackstar.configuration = self._load()
				app.blackstar.device_mapper.fill(app.blackstar.configuration)
			except Exception as e:
				app.blackstar.configuration = e
				LOGGER.critical('Failed to load configuration!', exc_info=e)

		return isinstance(app.blackstar.configuration, Configuration)

	@classmethod
	def _setup(cls, cfg: gunicorn.config.Config) -> None:
		setting = cfg.settings['post_worker_init']
		previous = setting.get()
		setting.set(functools.partial(cls._preload, previous=previous))

	@classmethod
	def _preload(cls, worker: gunicorn.workers.base.Worker, previous: typing.Callable[[gunicorn.workers.base.Worker], None]) -> None:
		namespace = getattr(worker.wsgi, 'blackstar', None)
		if namespace is not None:
			ensure = getattr(namespace, 'ensure_configuration', None)
			if ensure is not None:
				ensure()
		return previous(worker)

	@classmethod
	def _load(cls) -> Configuration:
		path = os.environ['BLACKSTAR_CONFIGURATION']
		return _load(path)

Logger.hooks.append(ConfigurationPlugin._setup)

def get_current() -> Configuration:
	result = bottle.request.app.blackstar.configuration
	if not isinstance(result, Configuration):
		raise TypeError('configuration not loaded or wrong type')
	return result

def get_mapper() -> DeviceMapper:
	return typing.cast(DeviceMapper, bottle.request.app.blackstar.device_mapper)
