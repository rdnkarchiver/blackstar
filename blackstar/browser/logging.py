import time
import typing
import traceback
import logging
import gunicorn.config
import gunicorn.glogging
import bottle

from ..common.logging import MESSAGE_FORMAT_PROCESS as MESSAGE_FORMAT, DATE_FORMAT, setup_logging

LOGGER = logging.getLogger('blackstar.browser')

class Logger(gunicorn.glogging.Logger): # type: ignore[misc]
	hooks: list[typing.Callable[[gunicorn.config.Config], None]] = []

	error_fmt = MESSAGE_FORMAT
	datefmt = DATE_FORMAT

	def setup(self, cfg: gunicorn.config.Config) -> None:
		super().setup(cfg)

		logfile = cfg.env.pop('BLACKSTAR_LOGFILE', None)
		setup_logging(logfile, self.loglevel, self.error_fmt, self.datefmt, False)

		for hook in self.hooks:
			hook(cfg)

class ExceptionPlugin:
	name = 'exception'
	api = 2

	def apply(self, callback: typing.Callable[..., typing.Any], route: bottle.Route) -> typing.Callable[..., typing.Any]:
		start = time.time()
		def wrapper(*args: typing.Any, **kwargs: typing.Any) -> typing.Any:
			try:
				return callback(*args, **kwargs)
			except bottle.BottleException:
				raise
			except Exception as e:
				LOGGER.error('An exception occured processing request:', exc_info=e)
				return bottle.HTTPError(500, 'Internal Server Error', e, traceback.format_exc())

		end = time.time()
		LOGGER.info('exception plugin took %.5f seconds', end - start)
		return wrapper
