import typing
import pathlib
import json
import urllib.parse
import bottle

from ...common.node import Node
from ..database import get_media_db
from ..application import master
from .utility import get_limiter, resolve_node

@master.route('/view/location', name='location')
def information_location() -> typing.Union[bottle.HTTPError, str]:
	if not bottle.request.token.can_info:
		return bottle.HTTPError(403, 'Information Restricted')

	bottle.response.set_header('Cache-Control', 'private, no-store')
	return bottle.template('information/location')

@master.route('/view/location.json', name='location_json', media=False, reference=False)
def information_location_json() -> typing.Union[bottle.HTTPError, str]:
	if not bottle.request.token.can_info:
		return bottle.HTTPError(403, 'Information Restricted')

	bottle.response.set_header('Cache-Control', 'private, no-store')
	bottle.response.content_type = 'application/json; charset=UTF-8'

	db = get_media_db()
	limiter = get_limiter()

	result: list[dict[str, typing.Any]] = []
	for row in db.execute('SELECT GROUP_CONCAT(device) AS devices, GROUP_CONCAT(inode) AS inodes, exif_latitude, exif_longitude FROM metadata WHERE exif_latitude IS NOT NULL AND exif_longitude IS NOT NULL GROUP BY exif_latitude, exif_longitude'):
		nodes: list[dict[str, typing.Any]] = []
		for node in (Node(device, inode) for device, inode in zip((int(device) for device in row['devices'].split(',')), (int(inode) for inode in row['inodes'].split(',')))):
			resolved = resolve_node(limiter, node)

			nodes.append({
				'names': resolved.names if resolved.names else [resolved.display],
				'url': urllib.parse.urljoin(bottle.request.url, bottle.request.app.get_url('node', device=node.device, inode=node.inode))
			})

		result.append({
			'nodes': nodes,
			'latitude': row['exif_latitude'],
			'longitude': row['exif_longitude']
		})

	return json.dumps(result)
