import os
import typing
import bottle

from .application import CONTENT_PATH, master

STATIC_PATH = os.path.join(CONTENT_PATH, 'static')
STATIC_SKIP = ['configuration', 'authorization', 'database']

@master.route('/favicon.ico', skip=STATIC_SKIP)
def serve_favicon_ico() -> bottle.HTTPResponse:
	bottle.redirect(bottle.request.app.get_url('/favicon.svg', pure=True))

@master.route('/favicon.svg', skip=STATIC_SKIP)
def serve_favicon_svg() -> bottle.HTTPResponse:
	return bottle.static_file('favicon.svg', CONTENT_PATH)

@master.route('/robots.txt', skip=STATIC_SKIP)
def serve_robotstxt() -> bottle.HTTPResponse:
	return bottle.static_file('robots.txt', CONTENT_PATH)

@master.route('/static/<path:path>', name='static', skip=STATIC_SKIP)
def serve_static(path: str) -> bottle.HTTPResponse:
	return bottle.static_file(path, STATIC_PATH);
