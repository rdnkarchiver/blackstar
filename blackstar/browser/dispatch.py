import os
import typing
import bottle

from ..common.reference import ID_LENGTH
from .authorization import TokenSource, update_environ
from .reference import resolve_validate
from .application import master
from .download import download
from .gallery import gallery

import time
from .logging import LOGGER

@master.route('/<id:re:[0-9A-Za-z]{{{}}}>'.format(ID_LENGTH), name='dispatch', anonymous=True, reference=True, media=False)
def dispatch(id: str) -> typing.Any:
	start = time.time()
	resolved = resolve_validate(id)

	if resolved.reference.token is not None:
		update_environ(resolved.reference.token)
		bottle.request.token = resolved.reference.token
		bottle.request.token_source = TokenSource.DATABASE

	ret = resolved.dispatch(file=download, directory=gallery)
	end = time.time()
	LOGGER.info('function dispatch() took %.5f seconds', end - start)
	return ret
