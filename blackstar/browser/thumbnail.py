import os
import typing
import bottle

from ..common.thumbnail import id_to_parts
from .configuration	import get_current as get_configuration
from .application import CONTENT_PATH, master

@master.route('/thumbnail/<path:path>', name='thumbnail', skip=['authorization', 'database'])
def serve_thumbnail(path: str) -> bottle.HTTPResponse:
	cfg = get_configuration()
	return bottle.static_file(path, os.fspath(cfg.directory.thumbnail))

def get_thumbnail_url(id: str, animated: bool) -> str:
	return typing.cast(str, bottle.request.app.get_url('thumbnail', path=os.fspath(id_to_parts(id, animated)), pure=True))
