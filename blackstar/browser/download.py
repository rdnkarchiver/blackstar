import os
import sys
import pathlib
import urllib.parse
import bottle

from .configuration import get_current as get_configuration
from .reference import ResolvedReference, update_environ, make_pure_path

def download(resolved: ResolvedReference) -> bottle.HTTPResponse:
	if not os.access(resolved.target, os.R_OK, follow_symlinks=False):
		return bottle.HTTPError(403, 'Target Path Forbidden')
	if not bottle.request.token.can_download:
		return bottle.HTTPError(403, 'Download Restricted')

	update_environ('download' if resolved.reference.download else 'view', resolved)

	cfg = get_configuration()
	if cfg.download.prefix is None:
		return bottle.static_file(os.fspath(resolved.target.relative_to(resolved.root)), os.fspath(resolved.root), download=resolved.reference.download)

	headers: dict[str, str] = {}
	headers['Cache-Control'] = '{}, no-cache, no-transform, must-revalidate'.format('private' if resolved.reference.token is None else 'public')

	disposition = 'attachment' if resolved.reference.download else 'inline'
	headers['Content-Disposition'] = make_content_disposition(disposition, resolved.target.name)

	relative =  make_pure_path(resolved.target)
	headers['X-Accel-Redirect'] = cfg.download.prefix + urllib.parse.quote(os.fspath(relative), encoding=sys.getfilesystemencoding(), errors=sys.getfilesystemencodeerrors())

	return bottle.HTTPResponse(b'', status=204, headers=headers)

def make_content_disposition(disposition: str, filename: str) -> str:
	try:
		return '{}; filename={}'.format(disposition, urllib.parse.quote(filename, encoding='ascii'))
	except UnicodeEncodeError:
		return '{}; filename*=UTF-8\'\'{}'.format(disposition, urllib.parse.quote(filename, encoding='utf-8', errors='ignore'))
