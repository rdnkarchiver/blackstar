import typing
import base64
import dataclasses
import hmac
import msgpack
import time

from ..common.configuration import User

VERSION = 0

ENCODE_MAPPING = {
	'version': ('v', int),
	'expiration': ('e', int),
	'username': ('u', str),
	'sequence': ('g', int),
	'email': ('m', str),
	'avatar': ('a', str),
	'root': ('r', str),
	'can_browse': ('b', bool),
	'can_download': ('d', bool),
	'can_share': ('s', bool),
	'can_xray': ('x', bool),
	'can_info': ('i', bool)
}

DECODE_MAPPING = {v[0]:(k, v[1]) for k,v in ENCODE_MAPPING.items()}

class TokenError(ValueError):
	pass

@dataclasses.dataclass(kw_only=True)
class Token:
	version: int = VERSION
	expiration: typing.Optional[int] = None

	username: typing.Optional[str] = None
	sequence: typing.Optional[int] = None
	email: typing.Optional[str] = None
	avatar: typing.Optional[str] = None
	root: typing.Optional[str] = None

	can_browse: typing.Optional[bool] = None
	can_download: typing.Optional[bool] = None
	can_share: typing.Optional[bool] = None
	can_xray: typing.Optional[bool] = None
	can_info: typing.Optional[bool] = None

	@classmethod
	def decode(cls, data: str, salt: bytes) -> 'Token':
		data = data + '=' * (0, 3, 2, 1)[len(data) % 4]
		bdata: bytes = base64.urlsafe_b64decode(data)

		binary = bdata[:-32]
		digest = bdata[-32:]
		if not hmac.compare_digest(digest, hmac.digest(salt, binary, 'sha256')):
			raise TokenError('invalid token signature')

		ddata: dict[typing.Any, typing.Any] = msgpack.unpackb(binary)
		kwargs = {}
		for k,v in ddata.items():
			property = DECODE_MAPPING.get(k, None)
			if property is None:
				continue
			kwargs[property[0]] = property[1](v)

		return cls(**kwargs)

	def encode(self, salt: bytes) -> str:
		kwargs = dataclasses.asdict(self)
		data = {ENCODE_MAPPING[k][0]:v for k,v in kwargs.items() if v is not None}
		binary = msgpack.packb(data)
		digest = hmac.digest(salt, binary, 'sha256')
		return base64.urlsafe_b64encode(binary + digest).rstrip(b'=').decode('ascii')

	@classmethod
	def from_user(cls, user: User, expiration: typing.Optional[int], root: typing.Optional[str]) -> 'Token':
		return cls(
			expiration=expiration,
			username=user.username,
			sequence=user.sequence,
			email=user.email,
			avatar=user.avatar,
			root=root,
			can_browse=user.can_browse or None,
			can_download=user.can_download or None,
			can_share=user.can_share or None,
			can_xray=user.can_xray or None,
			can_info=user.can_info or None)

	def is_expired(self, timer: typing.Callable[[], typing.Union[int, float]] = time.time) -> bool:
		if self.expiration is None:
			return False
		return timer() >= self.expiration

	def is_expired_at(self, time: typing.Union[int, float]) -> bool:
		if self.expiration is None:
			return False
		return time >= self.expiration

	@property
	def expired(self) -> bool:
		return self.is_expired()

	@property
	def anonymous(self) -> bool:
		return self.username is None

	@property
	def authenticated(self) -> bool:
		return self.username is not None
