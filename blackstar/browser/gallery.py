import os
import sys
import typing
import enum
import dataclasses
import pathlib
import functools
import datetime
import stat
import json
import urllib.parse
import shlex
import bottle

from .configuration import get_current as get_configuration, get_mapper
from .token import Token
from .database import get_reference_db, get_media_db
from .reference import Reference, ReferenceCreator, ResolvedReference, get_or_create_reference_nolock, update_environ, make_pure_path
from .template import fix_encoding, format_size, format_time
from .download import make_content_disposition
from .thumbnail import get_thumbnail_url

import time
from .logging import LOGGER

ReferenceFactory = typing.Callable[[pathlib.Path], Reference]

@dataclasses.dataclass(frozen=True)
class Fragment:
	reference: Reference
	display: str = ''

class Kind(enum.Enum):
	DIRECTORY = enum.auto()
	FILE = enum.auto()
	IMAGE = enum.auto()
	AUDIO = enum.auto()
	VIDEO = enum.auto()
	OTHER = enum.auto()

@dataclasses.dataclass(frozen=True)
class Entry:
	kind: Kind
	link: bool
	reference: typing.Optional[Reference]
	path: pathlib.Path
	hidden: bool
	display: str
	device: typing.Optional[int] = None
	inode: typing.Optional[int] = None
	size: typing.Optional[int] = None
	atime: typing.Optional[float] = None
	mtime: typing.Optional[float] = None
	ctime: typing.Optional[float] = None
	hash_md5: typing.Optional[bytes] = None
	hash_sha1: typing.Optional[bytes] = None
	hash_sha256: typing.Optional[bytes] = None
	width: typing.Optional[int] = None
	height: typing.Optional[int] = None
	duration: typing.Optional[float] = None
	vcodec: typing.Optional[str] = None
	acodec: typing.Optional[str] = None
	exif_make: typing.Optional[str] = None
	exif_model: typing.Optional[str] = None
	exif_software: typing.Optional[str] = None
	exif_latitude: typing.Optional[float] = None
	exif_longitude: typing.Optional[float] = None
	thumbnail_1x: typing.Optional[str] = None
	thumbnail_2x: typing.Optional[str] = None
	thumbnail_3x: typing.Optional[str] = None
	thumbnail_4x: typing.Optional[str] = None
	preview_1x: typing.Optional[str] = None
	preview_2x: typing.Optional[str] = None
	count_link: typing.Optional[int] = None
	count_similar: typing.Optional[int] = None
	count_duplicate: typing.Optional[int] = None

class Sort(enum.Enum):
	NAME = enum.auto()
	SIZE = enum.auto()
	TIME = enum.auto()

class Order(enum.Enum):
	ASCENDING = enum.auto()
	DESCENDING = enum.auto()

class View(enum.Enum):
	LIST = enum.auto()
	GRID = enum.auto()
	LARGE = enum.auto()
	TEXT = enum.auto()
	JSON = enum.auto()
	PLAYLIST = enum.auto()
	WGET = enum.auto()
	CURL = enum.auto()
	BBCODE = enum.auto()

@dataclasses.dataclass(frozen=True)
class Parameters:
	fragments: list[Fragment]
	entries: list[Entry]
	view: View
	sort: Sort
	order: Order
	mixed: bool

@dataclasses.dataclass(frozen=True)
class Handler:
	handler: typing.Callable[[Parameters], typing.Any]
	content_type: typing.Optional[str]
	extension: str

HANDLERS: dict[View, Handler] = {}

Sorter = typing.Callable[[Entry], tuple[typing.Any, ...]]

def gallery(resolved: ResolvedReference) -> typing.Any:
	start = time.time()
	if not os.access(resolved.target, os.R_OK|os.X_OK, follow_symlinks=False):
		return bottle.HTTPError(403, 'Target Path Forbidden')
	if not bottle.request.token.can_browse:
		return bottle.HTTPError(403, 'Browse Restricted')

	update_environ('browse', resolved)
	bottle.response.set_header('Cache-Control', 'private, no-store' if resolved.reference.token is None else 'no-store')

	with get_reference_db():
		factory = make_reference_factory(resolved.reference, get_or_create_reference_nolock)
		fragments = decompose(factory, resolved.root, resolved.target)
		entries = scan(factory, resolved.root, resolved.target)

	view = get_view_mode()
	sort, order, mixed = get_sort_info()
	entries.sort(key=get_sorter(sort, mixed), reverse=order==Order.DESCENDING)

	handler = HANDLERS[view]
	if handler.content_type:
		bottle.response.content_type = handler.content_type
	bottle.response.set_header('Content-Disposition', make_content_disposition('inline', fragments[-1].display or '-') + handler.extension)
	parameters = Parameters(fragments, entries, view, sort, order, mixed)
	result = handler.handler(parameters) # type: ignore

	end = time.time()
	LOGGER.info('function gallery() took %.5f seconds' % (end - start))
	return result

def view_html(parameters: Parameters) -> str:
	return typing.cast(str, bottle.template(
		'gallery/{}'.format(parameters.view.name.lower()),
		fragments=parameters.fragments,
		entries=parameters.entries,
		view=parameters.view,
		sort=parameters.sort,
		order=parameters.order,
		mixed=parameters.mixed))

HANDLERS[View.LIST] = Handler(view_html, None, '.html')
HANDLERS[View.GRID] = HANDLERS[View.LIST]
HANDLERS[View.LARGE] = HANDLERS[View.LIST]

def view_text(parameters: Parameters) -> str:
	return '\n'.join(href for href in {reference_to_href(entry.reference) for entry in parameters.entries} if href is not None)

HANDLERS[View.TEXT] = Handler(view_text, 'text/plain; charset=UTF-8', '.txt')

def view_json(parameters: Parameters) -> str:
	return json.dumps(list(map(entry_to_json, parameters.entries)))

HANDLERS[View.JSON] = Handler(view_json, 'application/json; charset=UTF-8', '.json')

def view_playlist(parameters: Parameters) -> str:
	result = ['#EXTM3U']
	for entry in parameters.entries:
		if entry.kind not in (Kind.AUDIO, Kind.VIDEO) or entry.reference is None:
			continue

		result.extend([
			'#EXTINF:{},{}'.format(int(entry.duration or 0.0), entry.display),
			typing.cast(str, reference_to_href(entry.reference))
		])

	return '\n'.join(result)

HANDLERS[View.PLAYLIST] = Handler(view_playlist, 'application/vnd.apple.mpegurl', '.m3u8')

def view_wget(parameters: Parameters) -> str:
	result = ['#!/bin/sh']
	for entry in parameters.entries:
		if entry.kind == Kind.DIRECTORY or entry.reference is None:
			continue

		result.append(' '.join(map(shlex.quote, ['wget', '-c', '-O', entry.display, typing.cast(str, reference_to_href(entry.reference))])))

	return '\n'.join(result)

HANDLERS[View.WGET] = Handler(view_wget, 'application/x-sh', '.sh')

def view_curl(parameters: Parameters) -> str:
	result = ['#!/bin/sh']
	for entry in parameters.entries:
		if entry.kind == Kind.DIRECTORY or entry.reference is None:
			continue

		result.append(' '.join(map(shlex.quote, ['curl', '-C', '-', '-f', '-o', entry.display, typing.cast(str, reference_to_href(entry.reference))])))

	return '\n'.join(result)

HANDLERS[View.CURL] = Handler(view_curl, 'application/x-sh', '.sh')

def view_bbcode(parameters: Parameters) -> str:
	return '\n'.join('[url={}]{}[/url]'.format(reference_to_href(entry.reference), entry.display) for entry in parameters.entries if entry.kind != Kind.DIRECTORY and entry.reference is not None)

HANDLERS[View.BBCODE] = Handler(view_bbcode, 'text/plain; charset=UTF-8', '.txt')

def get_parameter(name: str) -> typing.Optional[str]:
	value: typing.Optional[str] = bottle.request.query.get(name)
	if value is None:
		value = bottle.request.cookies.get(name)
	return value

TEnum = typing.TypeVar('TEnum', bound=enum.Enum)

def get_parameter_enum(name: str, type: typing.Type[TEnum], fallback: TEnum) -> TEnum:
	value = get_parameter(name)
	if value is None:
		return fallback
	evalue: typing.Optional[TEnum] = type.__members__.get(value.upper())
	if evalue is None:
		return fallback
	return evalue

def get_parameter_bool(name: str, fallback: bool) -> bool:
	value = get_parameter(name)
	if value is None:
		return fallback
	try:
		return bool(int(value))
	except ValueError:
		return fallback

def get_view_mode() -> View:
	return get_parameter_enum('view', View, View.LARGE)

def get_sort_info() -> tuple[Sort, Order, bool]:
	return get_parameter_enum('sort', Sort, Sort.NAME), get_parameter_enum('order', Order, Order.ASCENDING), get_parameter_bool('mixed', False)

def sorter_name(entry: Entry) -> tuple[typing.Any, ...]:
	return (entry.display.lower(),)

def sorter_size(entry: Entry, discriminator: Sorter = sorter_name) -> tuple[typing.Any, ...]:
	return (entry.size or 0, *discriminator(entry))

def sorter_time(entry: Entry, discriminator: Sorter = sorter_name) -> tuple[typing.Any, ...]:
	return (entry.mtime or 0.0, *discriminator(entry))

def sorter_demixer(entry: Entry, discriminator: Sorter = sorter_name) -> tuple[typing.Any, ...]:
	return (0 if entry.kind == Kind.DIRECTORY else 1, *discriminator(entry))

def get_sorter(sort: Sort, mixed: bool) -> Sorter:
	sorter: Sorter = sorter_name
	if sort == Sort.SIZE:
		sorter = functools.partial(sorter_size, discriminator=sorter)
	elif sort == Sort.TIME:
		sorter = functools.partial(sorter_time, discriminator=sorter)
	if not mixed:
		sorter = functools.partial(sorter_demixer, discriminator=sorter)
	return sorter

def is_hidden(display: str, extra: typing.Optional[frozenset[str]] = None) -> bool:
	if display.startswith('.'):
		return True
	elif extra is None:
		return False
	else:
		return display in extra

def get_small_icon(kind: Kind) -> str:
	if kind == Kind.DIRECTORY:
		return 'folder-open'
	elif kind == Kind.FILE:
		return 'file'
	elif kind == Kind.IMAGE:
		return 'image-o'
	elif kind == Kind.AUDIO:
		return 'audio-o'
	elif kind == Kind.VIDEO:
		return 'video-o'
	else:
		return 'file-o'

def get_large_icon(kind: Kind) -> str:
	if kind == Kind.DIRECTORY:
		return 'folder-open-o'
	elif kind == Kind.IMAGE:
		return 'picture-o'
	elif kind == Kind.AUDIO:
		return 'music'
	elif kind == Kind.VIDEO:
		return 'film'
	else:
		return 'file-o'

def get_info(entry: Entry) -> str:
	duration = int(entry.duration or 0.0)
	if entry.kind in (Kind.AUDIO, Kind.VIDEO) and duration:
		return str(datetime.timedelta(seconds=duration))

	if entry.width and entry.height:
		return '{}x{}'.format(entry.width, entry.height)

	return '-'

def get_summary(entry: Entry) -> str:
	lines: list[str] = []
	segments: list[str] = []

	if entry.width and entry.height:
		segments.append('{}x{}'.format(entry.width, entry.height))

	duration = int(entry.duration or 0.0)
	if duration:
		segments.append(str(datetime.timedelta(seconds=duration)))

	lines.append(' - '.join(segments))
	segments.clear()

	if entry.vcodec:
		segments.append(entry.vcodec)
	if entry.acodec:
		segments.append(entry.acodec)

	lines.append(' / '.join(segments))
	segments.clear()

	if entry.size:
		lines.append(format_size(entry.size))
	if entry.mtime:
		lines.append(format_time(entry.mtime))

	return '\n'.join([line for line in lines if line][:3])

def make_reference_factory(reference: Reference, creator: ReferenceCreator) -> ReferenceFactory:
	def factory(path: pathlib.Path) -> Reference:
		path_pure = make_pure_path(path)
		return creator(reference.token, path_pure, reference.download)
	return factory

def decompose(factory: ReferenceFactory, root: pathlib.Path, target: pathlib.Path) -> list[Fragment]:
	fragments: list[Fragment] = []
	while target != root:
		fragments.insert(0, Fragment(factory(target), fix_encoding(target.name)))
		target = target.parent

	fragments.insert(0, Fragment(factory(target)))
	active = False

	return fragments

def scan(factory: ReferenceFactory, root: pathlib.Path, target: pathlib.Path) -> list[Entry]:
	entries: list[Entry] = []

	if sys.platform == 'win32':
		def make_st_factory(entry: os.DirEntry[str]) -> typing.Callable[[], os.stat_result]:
			return functools.partial(os.lstat, entry.path)
	else:
		def make_st_factory(entry: os.DirEntry[str]) -> typing.Callable[[], os.stat_result]:
			return functools.partial(entry.stat, follow_symlinks=False)

	for entry in os.scandir(target):
		new_path = pathlib.Path(entry.path)
		new_entry = make_entry(factory, root, new_path, fix_encoding(entry.name), make_st_factory(entry))
		if new_entry is None:
			continue
		entries.append(new_entry)

	return entries

def make_entry(factory: ReferenceFactory, root: pathlib.Path, path: pathlib.Path, display: str, st_factory: typing.Callable[[], os.stat_result]) -> typing.Optional[Entry]:
	try:
		st = st_factory()
	except Exception:
		return make_other_entry(factory, False, path, display, None)

	if stat.S_ISDIR(st.st_mode):
		return make_directory_entry(factory, False, path, display, st, get_linked_stat(root, path))
	elif stat.S_ISREG(st.st_mode):
		return make_file_entry(factory, False, path, display, st)
	elif not stat.S_ISLNK(st.st_mode):
		return make_other_entry(factory, False, path, display, st)

	try:
		path = path.resolve(True)
	except Exception:
		return make_other_entry(factory, False, path, display, None)

	try:
		st = path.lstat()
	except Exception:
		return make_other_entry(factory, False, path, display, None)

	if not path.is_relative_to(root):
		return None

	if stat.S_ISDIR(st.st_mode):
		return make_directory_entry(factory, True, path, display, st, get_linked_stat(root, path))
	elif stat.S_ISREG(st.st_mode):
		return make_file_entry(factory, True, path, display, st)
	else:
		return make_other_entry(factory, True, path, display, st)

def get_linked_stat(root: pathlib.Path, path: pathlib.Path) -> typing.Optional[os.stat_result]:
	cfg = get_configuration()
	linked_path = path / cfg.extension.linked

	try:
		st = linked_path.lstat()
	except Exception:
		return None

	if stat.S_ISREG(st.st_mode):
		return st
	elif not stat.S_ISLNK(st.st_mode):
		return None

	try:
		linked_path = linked_path.resolve(True)
	except Exception:
		return None

	try:
		st = linked_path.lstat()
	except Exception:
		return None

	if not linked_path.is_relative_to(root):
		return None

	if stat.S_ISREG(st.st_mode):
		return st
	else:
		return None

def make_entry_kwargs_basic(kind: Kind, link: bool, reference: typing.Optional[Reference], path: pathlib.Path, hidden: bool, display: str) -> dict[str, typing.Any]:
	return {
		'kind': kind,
		'link': link,
		'reference': reference,
		'path': path,
		'hidden': hidden,
		'display': display
	}

def make_entry_kwargs_stat(st: typing.Optional[os.stat_result]) -> dict[str, typing.Any]:
	if not st:
		return {}

	result: dict[str, typing.Any] = {}

	if bottle.request.token.can_info:
		node = get_mapper().resolve(st)
		if node is not None:
			result.update({
				'device': node.device,
				'inode': node.inode
			})

	if stat.S_ISREG(st.st_mode):
		result['size'] = st.st_size

	result.update({
		'atime': st.st_atime,
		'mtime': st.st_mtime,
		'ctime': st.st_ctime
	})

	return result

def make_entry_kwargs_media(kind: Kind, st: os.stat_result) -> dict[str, typing.Any]:
	node = get_mapper().resolve(st)
	if node is None:
		return {}

	wanted: list[str] = []

	if kind != Kind.DIRECTORY:
		if bottle.request.token.can_download:
			wanted.extend([
				'hash_md5',
				'hash_sha1',
				'hash_sha256'
			])

		if kind == Kind.IMAGE:
			wanted.extend([
				'width',
				'height',
				'vcodec'
			])

		elif kind == Kind.AUDIO:
			wanted.extend([
				'duration',
				'acodec'
			])

		elif kind == Kind.VIDEO:
			wanted.extend([
				'width',
				'height',
				'duration',
				'vcodec',
				'acodec'
			])

		if kind in (Kind.IMAGE, Kind.VIDEO):
			if bottle.request.token.can_download or bottle.request.token.can_info:
				wanted.extend([
					'exif_make',
					'exif_model',
					'exif_software',
					'exif_latitude',
					'exif_longitude'
				])

	if kind in (Kind.DIRECTORY, Kind.IMAGE, Kind.VIDEO):
		wanted.extend([
			'thumbnail_1x',
			'thumbnail_2x',
			'thumbnail_3x',
			'thumbnail_4x'
		])

	if kind == Kind.VIDEO:
		wanted.extend([
			'preview_1x',
			'preview_2x'
		])

	if kind != Kind.DIRECTORY and bottle.request.token.can_info:
		wanted.extend([
			'count_link',
			'count_similar',
			'count_duplicate'
		])

	if not wanted:
		return {}

	db = get_media_db()
	stmt = 'SELECT {} FROM metadata WHERE device = ? AND inode = ? AND size = ? AND mtime = ? LIMIT 1'.format(', '.join(wanted))

	row = db.execute(stmt,
		(node.device,
		node.inode,
		st.st_size,
		st.st_mtime)).fetchone()

	if row is None:
		return {}

	return dict(row)

def make_directory_entry(factory: ReferenceFactory, link: bool, path: pathlib.Path, display: str, st: os.stat_result, linked_st: typing.Optional[os.stat_result]) -> typing.Optional[Entry]:
	cfg = get_configuration()
	reference = factory(path) if bottle.request.token.can_browse else None

	hidden = is_hidden(display, cfg.hidden.directory)
	if hidden and not bottle.request.token.can_xray:
		return None

	kwargs = make_entry_kwargs_basic(
		Kind.DIRECTORY,
		link,
		reference,
		path,
		hidden,
		display)

	kwargs.update(make_entry_kwargs_stat(st))

	if linked_st is not None:
		kwargs.update(make_entry_kwargs_media(Kind.DIRECTORY, linked_st))

	return Entry(**kwargs)

def make_file_entry(factory: ReferenceFactory, link: bool, path: pathlib.Path, display: str, st: os.stat_result) -> typing.Optional[Entry]:
	cfg = get_configuration()

	extension = os.path.splitext(display)[1].lower()
	if extension in cfg.extension.image:
		kind = Kind.IMAGE
	elif extension in cfg.extension.audio:
		kind = Kind.AUDIO
	elif extension in cfg.extension.video:
		kind = Kind.VIDEO
	else:
		kind = Kind.FILE

	reference = factory(path) if bottle.request.token.can_download else None

	hidden = is_hidden(display, cfg.hidden.file)
	if hidden and not bottle.request.token.can_xray:
		return None

	if display == cfg.extension.linked and cfg.hidden.linked:
		return None

	kwargs = make_entry_kwargs_basic(
		kind,
		link,
		reference,
		path,
		hidden,
		display)

	kwargs.update(make_entry_kwargs_stat(st))
	kwargs.update(make_entry_kwargs_media(kind, st))

	return Entry(**kwargs)

def make_other_entry(factory: ReferenceFactory, link: bool, path: pathlib.Path, display: str, st: typing.Optional[os.stat_result]) -> typing.Optional[Entry]:
	cfg = get_configuration()

	hidden = is_hidden(display)
	if hidden and not bottle.request.token.can_xray:
		return None

	kwargs = make_entry_kwargs_basic(
		Kind.OTHER,
		link,
		None,
		path,
		hidden,
		display)

	kwargs.update(make_entry_kwargs_stat(st))

	return Entry(**kwargs)

def reference_to_href(reference: typing.Optional[Reference]) -> typing.Optional[str]:
	if reference is None:
		return None

	return urllib.parse.urljoin(typing.cast(str, bottle.request.url), typing.cast(str, bottle.request.app.get_url('dispatch', id=reference.id)))

def hash_to_hex(hash: typing.Optional[bytes]) -> typing.Optional[str]:
	if hash is None:
		return None

	return hash.hex().upper()

def thumbnail_to_href(id: typing.Optional[str], animated: bool) -> typing.Optional[str]:
	if id is None:
		return None

	return urllib.parse.urljoin(typing.cast(str, bottle.request.url), get_thumbnail_url(id, animated))

def entry_to_json(entry: Entry) -> dict[str, typing.Any]:
	return remove_none({
		'kind': entry.kind.name.lower(),
		'link': entry.link,
		'href': reference_to_href(entry.reference),
		'hidden': entry.hidden,
		'display': entry.display,
		'device': entry.device,
		'inode': entry.inode,
		'size': entry.size,
		'atime': entry.atime,
		'mtime': entry.mtime,
		'ctime': entry.ctime,
		'hash': {
			'md5': hash_to_hex(entry.hash_md5),
			'sha1': hash_to_hex(entry.hash_sha1),
			'sha256': hash_to_hex(entry.hash_sha256)
		},
		'width': entry.width,
		'height': entry.height,
		'duration': entry.duration,
		'vcodec': entry.vcodec,
		'acodec': entry.acodec,
		'exif': {
			'make': entry.exif_make,
			'model': entry.exif_model,
			'software': entry.exif_software,
			'latitude': entry.exif_latitude,
			'longitude': entry.exif_longitude
		},
		'thumbnail': {
			'1x': thumbnail_to_href(entry.thumbnail_1x, False),
			'2x': thumbnail_to_href(entry.thumbnail_2x, False),
			'3x': thumbnail_to_href(entry.thumbnail_3x, False),
			'4x': thumbnail_to_href(entry.thumbnail_4x, False)
		},
		'preview': {
			'1x': thumbnail_to_href(entry.preview_1x, True),
			'2x': thumbnail_to_href(entry.preview_2x, True)
		},
		'count': {
			'link': entry.count_link,
			'similar': entry.count_similar,
			'duplicate': entry.count_duplicate
		}
	})

def remove_none(input: dict[str, typing.Any]) -> dict[str, typing.Any]:
	result = {}
	for k, v in input.items():
		if isinstance(v, dict):
			v = remove_none(v) or None
		if v is not None:
			result[k] = v
	return result
