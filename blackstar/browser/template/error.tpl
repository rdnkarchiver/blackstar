<%
from bottle import request, DEBUG as debug

title = 'Error {}'.format(error.status_code)
center = True

rebase('base')
%>
<h1>Error {{ error.status_code }}</h1>
%if error.status_code == 401:
<p class="uwu">Unauthorized &#x1F620;</p>
%elif error.status_code == 403:
<p class="uwu">Forbidden &#x1F60E;</p>
%elif error.status_code == 404:
<p class="uwu">Not found &#x1F605;</p>
%elif error.status_code in range(400, 499):
<p class="uwu">Don't do that &#x1F616;</p>
%elif error.status_code in range(500, 599):
<p class="uwu">Oops &#x1F618;</p>
%end
%if debug:
%if error.body:
<p class="uwu">{{ error.body }}</p>
%end
%if error.exception:
<pre>{{ repr(error.exception) }}</pre>
%end
%if error.traceback:
<details>
<summary>Traceback</summary>
<pre class="left">
{{ error.traceback.rstrip('\n') }}
</pre>
</details>
%end
%end
<p><a href="{{ request.app.get_url('root', pure=True) }}" class="uwu">Return</a></p>
