<%
from bottle import request

rebase('header')
%>
<span class="buttons">
%if request.token.can_share:
<button data-modal-open="share" data-share-id="{{ fragments[-1].reference.id }}" >
<span class="icon link"></span>
<span class="hidden-phone uwu">Share</span>
</button>
%end
<button data-modal-open="settings">
<span class="icon cog"></span>
<span class="hidden-phone uwu">Settings</span>
</button>
</span>
