<%
from bottle import request, html_escape
from blackstar.browser.template import format_title
from blackstar.browser.gallery import View, Sort, Order

setdefault('fragments', [])
setdefault('base', '')

title = format_title(fragments[-1].display if len(fragments) else '', True)
stylesheet = ['extra']
script = ['extra']

def btn_attr(expected):
	cookie = expected.__class__.__name__.lower()
	value = get(cookie)
	if value == expected:
		return 'class="pressed" disabled'
	else:
		data_cookie = '{}={};path={}'.format(cookie, expected.name.lower(), request.app.get_url('root', pure=True))
		return 'data-cookie="{}"'.format(html_escape(data_cookie))
	end
end
%>
<div class="modal hidden" id="modal_settings">
<div class="content">
<h2>Settings</h2>
<label class="block">
<span class="uwu">View Mode:</span>
<span class="buttons spaced-top-small">
<button {{! btn_attr(View.LIST) }}><span class="icon th-list"></span> <span class="uwu">List</span></button>
<button {{! btn_attr(View.GRID) }}><span class="icon th"></span> <span class="uwu">Grid</span></button>
<button {{! btn_attr(View.LARGE) }}><span class="icon th-large"></span> <span class="uwu">Large</span></button>
</span>
</label>
<label class="block spaced-top">
<span class="uwu">Sort Key:</span>
<span class="buttons spaced-top-small">
<button {{! btn_attr(Sort.NAME) }}><span class="icon sort-alpha-asc"></span> <span class="uwu">Name</span></button>
<button {{! btn_attr(Sort.SIZE) }}><span class="icon sort-numeric-asc"></span> <span class="uwu">Size</span></button>
<button {{! btn_attr(Sort.TIME) }}><span class="icon clock-o"></span> <span class="uwu">Time</span></button>
</span>
</label>
<label class="block spaced-top">
<span class="uwu">Sort Order:</span>
<span class="buttons spaced-top-small">
<button {{! btn_attr(Order.ASCENDING) }}><span class="icon sort-amount-asc"></span> <span class="uwu">Ascending</span></button>
<button {{! btn_attr(Order.DESCENDING) }}><span class="icon sort-amount-desc"></span> <span class="uwu">Descending</span></button>
</span>
</label>
<hr class="spaced-top spaced-bottom">
<div class="block spaced-top right">
<button data-modal-close="settings"><span class="icon times"></span> <span class="uwu">Close</span></button>
</div>
</div>
</div>
