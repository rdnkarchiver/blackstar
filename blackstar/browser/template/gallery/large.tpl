<%
from bottle import request
from blackstar.browser.thumbnail import get_thumbnail_url
from blackstar.browser.gallery import Kind, get_small_icon, get_large_icon, get_summary

setdefault('entries', [])

rebase('gallery/base')
%>
%for entry in entries:
<div class="item">
<div class="header">
<span class="icon {{ get_small_icon(entry.kind) }}"></span>
{{ entry.display }}
</div>
%if entry.reference is not None:
<a href="{{ request.app.get_url('dispatch', id=entry.reference.id) }}">
%end
<div class="thumbnail" title="{{ entry.display }}" data-video="{{ get_thumbnail_url(entry.preview_2x, True) if entry.preview_2x else '' }}">
%if entry.thumbnail_2x:
<img src="{{ get_thumbnail_url(entry.thumbnail_2x, False) }}" srcset="{{ '{} 2x'.format(get_thumbnail_url(entry.thumbnail_4x, False)) if entry.thumbnail_4x else '' }}" loading="lazy">
%else:
<span class="message icon {{ get_large_icon(entry.kind) }}"></span>
%end
%if (entry.exif_latitude and entry.exif_longitude) or entry.link or entry.hidden:
<div class="overlay left">\\
%if entry.exif_latitude and entry.exif_longitude:
<span class="pill info-location"><span class="icon map-marker"></span></span>\\
%end
%if entry.link:
<span><span class="icon external-link-square"></span></span>\\
%end
%if entry.hidden:
<span class="pill ghost"><span class="icon eye"></span></span>\\
%end
</div>
%end
%if entry.count_link or entry.count_similar or entry.count_duplicate:
<div class="overlay right">\\
%if entry.count_link:
<span class="pill count-link">{{ entry.count_link }}</span>\\
%end
%if entry.count_similar:
<span class="pill count-similar">{{ entry.count_similar }}</span>\\
%end
%if entry.count_duplicate:
<span class="pill count-duplicate">{{ entry.count_duplicate }}</span>\\
%end
</div>
%end
</div>
%if entry.reference is not None:
</a>
%end
<div class="footer">
<pre>
{{ get_summary(entry) }}
</pre>
%include('gallery/actions')
</div>
</div>
%end
