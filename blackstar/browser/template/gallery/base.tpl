<%
from bottle import request
from blackstar.browser.template import format_title
from blackstar.browser.gallery import View

setdefault('fragments', [])
setdefault('base', '')

title = format_title(fragments[-1].display if len(fragments) else '', True)
stylesheet = ['extra']
script = ['extra']

if request.token.can_share:
	script.append('share')
end
if view == View.GRID or view == View.LARGE:
	script.append('video')
end

rebase('base')
%>
%include('gallery/header', title=format_title(fragments[-1].display))
%include('gallery/path')
%include('gallery/settings')
%include('gallery/share')
<main class="files {{ view.name.lower() }}">
<h2 class="hidden">Contents</h2>
{{! base.rstrip('\n') }}
</main>
