%from bottle import request
<nav class="path">
<h2 class="hidden">Path</h2>
<ol class="path">\\
%for i, fragment in enumerate(fragments):
<li><wbr>\\
%if i == len(fragments) - 1:
<strong>\\
%else:
<a href="{{ request.app.get_url('dispatch', id=fragment.reference.id) }}">\\
%end
%if i == 0:
<span class="icon home"></span><span class="hidden">ROOT</span>\\
%else:
{{ fragment.display }}\\
%end
%if i == len(fragments) - 1:
</strong>\\
%else:
</a>\\
%end
</li>\\
%end
</ol>
</nav>
