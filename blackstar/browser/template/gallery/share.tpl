%from urllib.parse import urljoin
%from bottle import request
%from blackstar.common.reference import ID_LENGTH
%if request.token.can_share:
<div class="modal hidden" id="modal_share" data-endpoint="{{ request.app.get_url('share') }}" data-id="{{ fragments[-1].reference.id }}">
<div class="content">
<h2>Share</h2>
<label class="block">
<span class="uwu">Expiration:</span>
<input class="spaced-left" id="share_expiration" type="number" size="4" value="1">
<select class="spaced-left-small" id="share_expiration_multiplier">
<option value="1">second(s)</option>
<option value="60">minute(s)</option>
<option value="3600" selected>hour(s)</option>
<option value="86400">day(s)</option>
<option value="604800">week(s)</option>
<option value="2592000">month(s)</option>
</select>
</label>
<label class="block spaced-top">
<span class="uwu">Download:</span>
<select class="spaced-left" id="share_download">
<option selected>allow</option>
<option>force</option>
<option>restrict</option>
</select>
</label>
<div class="block spaced-top">
<input type="url" id="share_url" size="32" readonly placeholder="{{ urljoin(request.url, request.app.get_url('dispatch', id='x' * ID_LENGTH, pure=True)) }}"><button class="spaced-left-small" id="share_copy">
<span class="icon files-o"></span>
<span class="hidden uwu">Copy</span>
</button>
</div>
<hr class="spaced-top spaced-bottom">
<div class="block spaced-top right">
<span class="icon spinner hidden" id="share_spinner"></span><button id="share_start"><span class="icon link"></span> <span class="uwu">Share</span></button><button class="spaced-left" data-modal-close="share"><span class="icon times"></span> <span class="uwu">Close</span></button>
</div>
</div>
</div>
%end
