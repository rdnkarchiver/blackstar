<%
from bottle import request

location_shown = entry.exif_latitude and entry.exif_longitude
download_shown = entry.kind != Kind.DIRECTORY and entry.reference is not None and not entry.reference.download
share_shown = request.token.can_share and entry.reference is not None
info_shown = entry.device is not None and entry.inode is not None
%>
%if location_shown or download_shown or share_shown or info_shown:
<span class="actions">\\
%if location_shown:
<a class="button minimal" href="https://www.openstreetmap.org/?mlat={{ entry.exif_latitude }}&mlon={{ entry.exif_longitude }}" title="location"><span class="icon map-marker"></span><span class="hidden uwu">Location</span></a>\\
%end
%if download_shown:
<a class="button minimal" href="{{ request.app.get_url('dispatch', id=entry.reference.id) }}" download title="download"><span class="icon download"></span><span class="hidden uwu">Download</span></a>\\
%end
%if share_shown:
<button class="minimal" data-modal-open="share" data-share-id="{{ entry.reference.id }}" title="share"><span class="icon link"></span><span class="hidden uwu">Share</span></button>\\
%end
%if info_shown:
<a class="button minimal" href="{{ request.app.get_url('node', device=entry.device, inode=entry.inode) }}" title="information"><span class="icon info-circle"></span><span class="hidden uwu">Information</span></a>\\
%end
</span>
%end
