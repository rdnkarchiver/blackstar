<%
from bottle import request
from blackstar.browser.thumbnail import get_thumbnail_url
from blackstar.browser.gallery import Entry, get_large_icon

setdefault('entries', [])

rebase('gallery/base')

def make_srcset(entry: Entry) -> str:
	result = []
	if entry.thumbnail_2x:
		result.append('{} 2x'.format(get_thumbnail_url(entry.thumbnail_2x, False)))
	end
	if entry.thumbnail_3x:
		result.append('{} 3x'.format(get_thumbnail_url(entry.thumbnail_3x, False)))
	end
	if entry.thumbnail_4x:
		result.append('{} 4x'.format(get_thumbnail_url(entry.thumbnail_4x, False)))
	end
	return ', '.join(result)
end
%>
%for entry in entries:
<div class="item" title="{{ entry.display }}">
%if entry.reference is not None:
<a href="{{ request.app.get_url('dispatch', id=entry.reference.id) }}">
%end
<div class="thumbnail{{ ' ghost' if entry.hidden else '' }}" data-video="{{ get_thumbnail_url(entry.preview_1x, True) if entry.preview_1x else '' }}">
%if entry.thumbnail_1x:
<img src="{{ get_thumbnail_url(entry.thumbnail_1x, False) }}" srcset="{{ make_srcset(entry) }}" loading="lazy">
%else:
<span class="message icon {{ get_large_icon(entry.kind) }}"></span>
%end
%if entry.exif_latitude and entry.exif_longitude:
<div class="overlay left"><span class="icon circle info-location"></span></div>
%end
%if entry.count_link or entry.count_similar or entry.count_duplicate:
<div class="overlay right">\\
%if entry.count_link:
<span class="icon circle count-link"></span>\\
%end
%if entry.count_similar:
<span class="icon circle count-similar"></span>\\
%end
%if entry.count_duplicate:
<span class="icon circle count-duplicate"></span>\\
%end
</div>
%end
</div>
<div class="footer">\\
%if entry.link:
<span class="icon external-link-square"></span> \\
%end
{{ entry.display }}</div>
%if entry.reference is not None:
</a>
%end
</div>
%end
