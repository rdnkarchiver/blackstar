%from math import cos, sin, pi, tau
%rebase('information/base', title='Database')
%def render_data(data):
%graph_rows = [row for row in data.rows if row.ratio is not None]
%if graph_rows:
<div class="table-chart">
%end
<table class="striped">
<colgroup>
%for column in data.columns:
%if column.visible:
<col>
%end
%end
</colgroup>
<thead>
<tr class="uwu">
%for column in data.columns:
%if column.visible:
<th>{{ column.display }}</th>
%end
%end
</tr>
</thead>
<tbody>
%for row in data.rows:
%if row.visible:
<tr>
%for column, value in zip(data.columns, row.values):
%if column.visible:
%if column.lower:
<td class="uwu">\\
%else:
<td>\\
%end
%if column.tooltip:
<span title="{{ value }}">\\
%end
{{ column.formatter(value) }}\\
%if column.tooltip:
</span>\\
%end
</td>
%end
%end
</tr>
%end
%end
</tbody>
</table>
%if graph_rows:
%graph_rows.sort(key=lambda row: -row.ratio)
%radius = 128
%radius_inner = radius - 2
%def get_xy(angle):
%return round(cos(angle) * radius_inner, 3), -round(sin(angle) * radius_inner, 3)
%end
<div class="chart">
<svg width="{{ radius * 2 }}" height="{{ radius * 2 }}" viewbox="{{ -radius }} {{ -radius }} {{ radius * 2 }} {{ radius * 2 }}">
%start_angle = pi / 2.0
%x1, y1 = get_xy(start_angle)
%points = []
%for index, row in enumerate(graph_rows):
%delta_angle = row.ratio * tau
%end_angle = start_angle + delta_angle
%x2, y2 = get_xy(end_angle)
%points.append((x2, y2))
%if row.ratio < 1.0:
<path class="slice slice-{{ index }}" d="M 0 0 L {{ x1 }} {{ y1 }} A {{ radius_inner }} {{ radius_inner }} 0 {{ int(delta_angle > pi) }} 0 {{ x2 }} {{ y2 }}" fill="#c0c0c0">
%else:
<circle class="slice slice-{{ index }}" cx="0" cy="0" r="{{ radius_inner }}" fill="#c0c0c0">
%end
%if row.display is not None:
<title>{{ '{} ({:.1f}%)'.format(row.display.lower() if row.dlower else row.display, row.ratio * 100.0) }}</title>
%end
%if row.ratio < 1.0:
</path>
%else:
</circle>
%end
%start_angle = end_angle
%x1, y1 = x2, y2
%end
<path class="overlay" d="{{ ' '.join('M 0 0 L {} {}'.format(*point) for point in points) }}" fill="none" stroke="black" stroke-width="1px" />
<circle class="overlay" cx="0" cy="0" r="{{ radius_inner }}" fill="none" stroke="black" stroke-width="1px" />
</svg>
<ul>
%for index, row in enumerate(graph_rows):
%display = 'Value #{}'.format(index + 1) if row.display is None else row.display
%cls = ['slice', 'slice-{}'.format(index)]
%if row.dlower:
%cls.append('uwu')
%end
<li class="{{ ' '.join(cls) }}">{{ display }}</li>
%end
</ul>
</div>
</div>
%end
%end
<main>
%if database or task:
<h2>Database</h2>
%if database:
<h3>Files</h3>
%render_data(database)
%end
%if task:
<h3>Tasks</h3>
%render_data(task)
%end
%end
%if redundancy:
<h2>Redundancy</h2>
%render_data(redundancy)
%end
%if video or audio or image or resv or resi:
<h2>Metadata</h2>
%if video:
<h3>Top Video Codecs</h3>
%render_data(video)
%end
%if image:
<h3>Top Image Codecs</h3>
%render_data(image)
%end
%if audio:
<h3>Top Audio Codecs</h3>
%render_data(audio)
%end
%if resv:
<h3>Top Video Resolutions</h3>
%render_data(resv)
%end
%if resi:
<h3>Top Image Resolutions</h3>
%render_data(resi)
%end
%end
</main>
