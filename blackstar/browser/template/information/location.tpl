<%
from bottle import request

title = 'Location'
stylesheet = ['extra', 'ol']
script = ['extra', 'ol', 'map']

rebase('information/base')
%>
<div class="map" data-uri="{{ request.app.get_url('location_json') }}"><div class="overlay"></div></div>
