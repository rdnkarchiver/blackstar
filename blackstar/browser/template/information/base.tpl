<%
from blackstar.browser.template import format_title

setdefault('title', None)
setdefault('stylesheet', ['extra'])
setdefault('script', ['extra'])

rebase('base', title=format_title(title, True))
%>
%include('information/header')
{{! base.rstrip('\n') }}
