<%
import datetime
from bottle import request, html_escape
from blackstar.browser.template import format_size, format_time
from blackstar.browser.gallery import Kind

rebase('information/base', title=entry.display)
%>
<main>
<h2>File</h2>
<table>
<colgroup>
<col>
<col>
<col>
<col>
</colgroup>
<tbody>
<tr>
<th class="uwu">Device</th>
<td>{{ entry.device }}</td>
<th class="uwu">INode</th>
<td>{{ entry.inode }}</td>
</tr>
<tr>
<th class="uwu">Size</th>
<td><span title="{{ entry.size }}">{{ format_size(entry.size) }}</span></td>
<th class="uwu">Kind</th>
<td class="uwu">{{ entry.kind.name.capitalize() }}</td>
</tr>
<tr>
<th class="uwu">Accessed</th>
<td colspan="3"><span title="{{ entry.atime }}">{{ format_time(entry.atime) }}</span></td>
</tr>
<tr>
<th class="uwu">Modified</th>
<td colspan="3"><span title="{{ entry.mtime }}">{{ format_time(entry.mtime) }}</span></td>
</tr>
<tr>
<th class="uwu">Changed</th>
<td colspan="3"><span title="{{ entry.ctime }}">{{ format_time(entry.ctime) }}</span></td>
</tr>
%if entry.hash_md5 is not None:
<tr>
<th class="uwu">MD5</th>
<td colspan="3"><code>{{ entry.hash_md5.hex().upper() }}</code></td>
</tr>
%end
%if entry.hash_sha1 is not None:
<tr>
<th class="uwu">SHA1</th>
<td colspan="3"><code>{{ entry.hash_sha1.hex().upper() }}</code></td>
</tr>
%end
%if entry.hash_sha256 is not None:
<tr>
<th class="uwu">SHA256</th>
<td colspan="3"><code>{{ entry.hash_sha256.hex().upper() }}</code></td>
</tr>
%end
</tbody>
</table>
%if entry.width or entry.height or entry.vcodec or entry.acodec or entry.duration or entry.exif_make or entry.exif_model or entry.exif_software or (entry.exif_latitude and entry.exif_longitude):
<h2>Media</h2>
<table>
<colgroup>
<col>
<col>
<col>
<col>
</colgroup>
%if entry.width or entry.height:
<tr>
<th class="uwu">Width</th>
<td>{{ entry.width or '-' }}</td>
<th class="uwu">Height</th>
<td>{{ entry.height or '-' }}</td>
</tr>
%end
%if entry.vcodec or entry.acodec:
<tr>
<th class="uwu">Video</th>
<td><code>{{ entry.vcodec or '-' }}</code></td>
<th class="uwu">Audio</th>
<td><code>{{ entry.acodec or '-' }}</code></td>
</tr>
%end
%if entry.duration:
<tr>
<th class="uwu">Duration</th>
<td colspan="3"><span title="{{ entry.duration }}">{{ datetime.timedelta(seconds=entry.duration) }}</span></td>
</tr>
%end
%if entry.exif_make:
<tr>
<th class="uwu">Make</th>
<td colspan="3"><code>{{ entry.exif_make }}</code></td>
</tr>
%end
%if entry.exif_model:
<tr>
<th class="uwu">Model</th>
<td colspan="3"><code>{{ entry.exif_model }}</code></td>
</tr>
%end
%if entry.exif_software:
<tr>
<th class="uwu">Software</th>
<td colspan="3"><code>{{ entry.exif_software }}</code></td>
</tr>
%end
%if entry.exif_latitude and entry.exif_longitude:
<tr>
<th class="uwu">Location</th>
<td colspan="3"><a class="button minimal" href="https://www.openstreetmap.org/?mlat={{ entry.exif_latitude }}&mlon={{ entry.exif_longitude }}"><span class="icon map-marker"></span><span class="hidden uwu">Show</span></a> {{ entry.exif_latitude }}, {{ entry.exif_longitude }}</td>
</tr>
%end
<tbody>
</tbody>
</table>
%end
%if links:
<h2>Links</h2>
<ul>
%link_last = request.token.can_download or entry.kind == Kind.DIRECTORY
%for fragments in links:
<li><ol class="path">\\
%for i, fragment in enumerate(fragments):
<li>\\
%if link_last or i != len(fragments) - 1:
<a href="{{ request.app.get_url('dispatch', id=fragment.reference.id) }}">\\
%end
%if i == 0:
<span class="icon home"></span><span class="hidden">ROOT</span>\\
%else:
{{ fragment.display }}\\
%end
%if link_last or i != len(fragments) - 1:
</a>\\
%end
</li>\\
%end
</ol></li>
%end
</ul>
%end
%if others:
<h2>Other</h2>
<ul>
%for other in others:
<li><span class="icon circle {{ 'count-duplicate' if other.distance < 0 else 'count-similar' }}" title="{{ 'duplicate' if other.distance < 0 else 'similar: {:.0f}%'.format((64 - other.distance) / 0.64) }}"></span> \\
%if len(other.names) > 1:
<span title="{{! '&#x000a;'.join(html_escape(name) for name in other.names) }}">\\
%end
<a href="{{ request.app.get_url('node', device=other.node.device, inode=other.node.inode) }}">{{ other.display }}</a>\\
%if len(other.names) > 1:
 (+{{ len(other.names) - 1 }})</span>\\
%end
</li>
%end
</ul>
%end
</main>
