"use strict";

function thumbnail_video_start(thumbnail)
{
	thumbnail.videoTimer = null;

	const img = thumbnail.querySelector("img");
	if (img)
		img.classList.add("hidden");

	const message = thumbnail.querySelector("span.message");
	if (message)
		message.classList.add("hidden");

	const video = document.createElement("video");
	video.src = thumbnail.dataset.video;
	video.loop = true;
	video.autoplay = true;
	video.muted = true;

	if (img)
		video.poster = img.src;

	thumbnail.insertBefore(video, img || message || thumbnail.firstElementChild);

	thumbnail.videoShown = true;
}

function thumbnail_video_stop(thumbnail)
{
	if (!thumbnail.videoShown)
		return;

	const video = thumbnail.querySelector("video");
	if (video)
		thumbnail.removeChild(video);

	const message = thumbnail.querySelector("span.message");
	if (message)
		message.classList.remove("hidden");

	const img = thumbnail.querySelector("img");
	if (img)
		img.classList.remove("hidden");

	thumbnail.videoShown = false;
}

function thumbnail_video_enter(event)
{
	const thumbnail = event.currentTarget;

	if (thumbnail.videoTimer || thumbnail.videoShown)
		return;

	thumbnail.videoTimer = setTimeout(thumbnail_video_start, 1000, thumbnail);
	event.preventDefault();
}

function thumbnail_video_leave(event)
{
	const thumbnail = event.currentTarget;

	if (thumbnail.videoTimer)
	{
		clearTimeout(thumbnail.videoTimer);
		thumbnail.videoTimer = null;
	}

	thumbnail_video_stop(thumbnail);
	event.preventDefault();
}

for (const thumbnail of document.querySelectorAll("div.thumbnail[data-video]"))
{
	if (!thumbnail.dataset.video)
		continue;

	thumbnail.videoTimer = null;
	thumbnail.videoShown = false;
	thumbnail.addEventListener('mouseenter', thumbnail_video_enter);
	thumbnail.addEventListener('mouseleave', thumbnail_video_leave);
}
