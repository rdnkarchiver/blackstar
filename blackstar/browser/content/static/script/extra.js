"use strict";

function button_navigate(event)
{
	const button = event.currentTarget;
	const uri = button.dataset.uri;
	window.location.assign(uri);
	event.preventDefault();
}

function button_setcookie(event)
{
	const button = event.currentTarget;
	const cookie = button.dataset.cookie;
	document.cookie = cookie;
	window.location.reload();
	event.preventDefault();
}

function button_modal_open(event)
{
	const button = event.currentTarget;
	const modal = button.dataset.modalOpen;
	document.body.classList.add("modal");

	const modal2 = document.querySelector("div.modal#modal_" + modal);
	modal2.classList.remove("hidden");

	const input = modal2.querySelector("input");
	if (input)
	{
		input.select();
	}
	else
	{
		const button = modal2.querySelector("button");
		if (button)
			button.focus();
	}

	event.preventDefault();
}

function button_modal_close(event)
{
	const button = event.currentTarget;
	const modal = button.dataset.modalClose;
	document.querySelector("div.modal#modal_" + modal).classList.add("hidden");
	document.body.classList.remove("modal");
	event.preventDefault();
}

function hookup(selector, listener, type="click")
{
	for (const button of document.querySelectorAll(selector))
		button.addEventListener(type, listener);
}

hookup("button[data-uri]", button_navigate);
hookup("button[data-cookie]", button_setcookie);
hookup("button[data-modal-open]", button_modal_open);
hookup("button[data-modal-close]", button_modal_close);
