"use strict";

const container = document.querySelector("div.map");
const overlay = document.querySelector("div.overlay");

const popup = new ol.Overlay({
	element: overlay,
	positioning: "bottom-center"
});

const markers = new ol.source.Vector();

const map = new ol.Map({
	target: container,
	controls: ol.control.defaults({
		attribution: false
	}),
	layers: [
		new ol.layer.Tile({
			source: new ol.source.OSM()
		}),
		new ol.layer.Vector({
			source: markers,
			style: new ol.style.Style({
				image: new ol.style.Circle({
					fill: new ol.style.Fill({ color: '#a040ffc0' }),
					radius: 5
				})
			})
		})
	],
	overlays: [ popup ],
	view: new ol.View({
		center: ol.proj.fromLonLat([10.0, 40.0]),
		zoom: 2
	})
});

const request = new XMLHttpRequest();

request.onload = function(event)
{
	const data = JSON.parse(request.responseText);

	for (const d of data)
	{
		const marker = new ol.Feature({
			geometry: new ol.geom.Point(ol.proj.fromLonLat([d.longitude, d.latitude])),
			nodes: d.nodes
		});

		markers.addFeature(marker);
	}

	event.preventDefault();
};

request.open("GET", container.dataset.uri);
request.send();

map.on("click", function(event)
{
	const features = map.getFeaturesAtPixel(event.pixel);
	if (!features.length)
	{
		popup.setPosition(undefined);
		return;
	}

	while (overlay.firstChild)
		overlay.removeChild(overlay.lastChild);

	const all_nodes = new Array();
	for (const feature of features)
	{
		const feature_nodes = feature.get("nodes");
		for (const node in feature_nodes)
			all_nodes.push(feature_nodes[node])
	}

	all_nodes.sort(function(a, b)
	{
		const n1 = a.names[0].toLowerCase();
		const n2 = b.names[0].toLowerCase();

		if (n1 > n2)
			return 1;
		else if (n1 < n2)
			return -1;
		else
			return 0;
	});

	const div = document.createElement("div");
	const ul = document.createElement("ul");

	for (const node of all_nodes)
	{
		const li = document.createElement("li");

		const a = document.createElement("a");
		a.href = node.url;
		a.appendChild(document.createTextNode(node.names[0]));
		li.appendChild(a);

		if (node.names.length > 1)
		{
			const text = document.createTextNode(` (+${node.names.length - 1})`);
			li.title = node.names.join('\n');
			li.appendChild(text);
		}

		ul.appendChild(li);
	}

	div.appendChild(ul);
	overlay.appendChild(div);

	popup.setPosition(event.coordinate);
});

map.on("pointermove", function(event)
{
	container.style.cursor = map.hasFeatureAtPixel(event.pixel) ? "pointer" : "";
});
