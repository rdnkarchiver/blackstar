function share_open(event)
{
	const button = event.currentTarget;
	const modal = document.querySelector("div.modal#modal_share");

	modal.dataset.id = button.dataset.shareId;

	const textbox = modal.querySelector("input#share_url")
	textbox.value = '';
}

function share_start(event)
{
	const data = new FormData();
	const modal = document.querySelector("div.modal#modal_share");

	data.append("id", modal.dataset.id);
	data.append("expiration", parseInt(modal.querySelector("input#share_expiration").value) * parseInt(modal.querySelector("select#share_expiration_multiplier").value));
	data.append("download", modal.querySelector("select#share_download").value);

	modal.querySelector("button#share_start").classList.add("hidden")
	modal.querySelector("span#share_spinner").classList.remove("hidden");

	const request = new XMLHttpRequest();
	request.onload = share_end;
	request.onerror = share_end;
	request.ontimeout = share_end;
	request.open("POST", modal.dataset.endpoint);

	request.send(data);

	event.preventDefault();
}

function share_end(event)
{
	const location = this.getResponseHeader("Location");
	const modal = document.querySelector("div.modal#modal_share");
	const textbox = modal.querySelector("input#share_url")

	if (location)
	{
		textbox.value = location;
		textbox.select();
	}
	else
	{
		textbox.value = '';
	}

	modal.querySelector("span#share_spinner").classList.add("hidden");
	modal.querySelector("button#share_start").classList.remove("hidden")

	if (!location)
		alert("share error");

	event.preventDefault();
}

function share_copy(event)
{
	const textbox = document.querySelector("input#share_url");
	textbox.select();
	document.execCommand('copy');
	event.preventDefault();
}

for (const button of document.querySelectorAll("button[data-modal-open=\"share\"][data-share-id]"))
	button.addEventListener("click", share_open)

document.querySelector("button#share_start").addEventListener("click", share_start);
document.querySelector("button#share_copy").addEventListener("click", share_copy);
