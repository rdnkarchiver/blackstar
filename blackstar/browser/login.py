import typing
import crypt
import hmac
import pathlib
import bottle

from .logging import LOGGER
from .configuration import get_current as get_configuration
from .token import Token
from .authorization import TokenSource, set_token_cookie
from .reference import get_or_create_reference, make_pure_path
from .application import master

@master.route('/', name='root', anonymous=True, reference=True)
def serve_root() -> typing.Optional[str]:
	bottle.response.set_header('Cache-Control', 'no-store')

	token: Token = bottle.request.token
	if token is None:
		return bottle.template('login')

	bottle.response.set_header('Cache-Control', 'private, no-store')

	if token.root is not None:
		bottle.redirect(bottle.request.app.get_url('dispatch', id=token.root))

	reference = get_or_create_reference(None, pathlib.PurePosixPath(), False)
	bottle.redirect(bottle.request.app.get_url('dispatch', id=reference.id))
	return None

@master.route('/action/login', name='login', method='POST', anonymous=True, reference=True)
def login() -> typing.Optional[bottle.HTTPError]:
	bottle.response.set_header('Cache-Control', 'private, no-store')
	username = bottle.request.forms.getunicode('username', '')
	password = bottle.request.forms.getunicode('password', '')
	remember = bool(bottle.request.forms.getunicode('remember', ''))

	token = authenticate(username, password)
	if token is None:
		return bottle.HTTPError(401, 'Invalid Username or Password')

	set_token_cookie(token, remember)
	bottle.redirect(bottle.request.app.get_url('root', pure=True))
	return None

@master.route('/action/logout', name='logout', method=['GET', 'POST'])
def logout() -> typing.Optional[bottle.HTTPError]:
	bottle.response.set_header('Cache-Control', 'private, no-store')
	if bottle.request.token_source != TokenSource.COOKIE:
		return bottle.HTTPError(400, 'Invalid Token Source')

	set_token_cookie(None, False)
	bottle.redirect(bottle.request.app.get_url('root', pure=True))
	return None

def get_caller_name(check_token: bool = True) -> str:
	if check_token and bottle.request.token.username:
		return typing.cast(str, bottle.request.token.username)

	source: typing.Optional[str] = bottle.request.environ.get('HTTP_X_REAL_IP', None)
	if not source:
		source = bottle.request.environ.get('REMOTE_ADDR', None)
	if not source:
		source = '<unknown>'

	return source

def authenticate(username: str, password: str) -> typing.Optional[Token]:
	cfg = get_configuration()
	caller = get_caller_name(False)

	user = cfg.users.get(username)
	if not user:
		LOGGER.warning('LOGIN FAILED: unknown username from %s', caller)
		return None

	password = crypt.crypt(password, user.password)
	if not hmac.compare_digest(password, user.password):
		LOGGER.error('LOGIN FAILED: invalid password for %s from %s', username, caller)
		return None

	root = user.root
	if not user.root.is_relative_to(cfg.directory.root):
		root = cfg.directory.root

	pure_root = make_pure_path(root)
	root_id: typing.Optional[str] = None
	if pure_root.parts:
		reference = get_or_create_reference(None, pure_root, False)
		root_id = reference.id

	LOGGER.info('LOGIN: %s from %s', username, caller)
	return Token.from_user(user, bottle.request.time + cfg.authentication.expiration, root_id)
