import os
import typing
import subprocess
import locale
import json

from .logging import LOGGER
from .utility import try_lstat, try_fstat

def report_failure(args: list[str], stderr_data: typing.Union[str, bytes, None], fmt: str, *a: typing.Any, **kw: typing.Any) -> None:
	fmtargs = list(a)

	fmt += '\nArguments: %r'
	fmtargs.append(args)

	if stderr_data:
		fmt += '\nStd Error: %r'
		fmtargs.append(stderr_data)

	LOGGER.error(fmt, *fmtargs, *kw)

class ExternalProcess(subprocess.CompletedProcess[typing.Union[bytes, str]]):
	def __init__(self, args: list[str], returncode: int, stdout: typing.Union[bytes, str, None], stderr: typing.Union[bytes, str, None], json: typing.Any) -> None:
		super().__init__(args, returncode, stdout, stderr)
		self.json = json

def try_execute(
	args: list[str],
	stdout: typing.Union[int, typing.BinaryIO, None] = None,
	cwd: typing.Union[str, os.PathLike[str], None] = None,
	env: typing.Optional[dict[str, str]] = None,
	encoding: typing.Optional[str] = None,
	errors: typing.Optional[str] = None,
	check: bool = True,
	check_stdout: bool = False,
	check_path: typing.Union[str, os.PathLike[str], None] = None,
	check_fd: typing.Optional[int] = None) -> typing.Optional[ExternalProcess]:

	process: subprocess.Popen[bytes]
	try:
		process = subprocess.Popen(
			args=args,
			stdin=subprocess.DEVNULL,
			stdout=stdout,
			stderr=subprocess.PIPE,
			cwd=cwd, env=env,
			start_new_session=True)
	except OSError as e:
		report_failure(args, None, 'Failed to execute external process: %s', e.strerror)
		return None
	except Exception as e:
		report_failure(args, None, 'Failed to execute external process:', exc_info=e)
		return None

	with process:
		stdout_data: typing.Union[bytes, str, None]
		stderr_data: typing.Union[bytes, str]

		try:
			stdout_data, stderr_data = process.communicate()
		except:
			process.kill()
			raise

		code: int = process.poll() # type: ignore

		default_encoding = locale.getpreferredencoding(False)

		try:
			assert isinstance(stderr_data, bytes)
			stderr_data = stderr_data.decode(default_encoding)
		except UnicodeDecodeError:
			pass

		if check and code:
			report_failure(args, stderr_data, 'External process exited with code: %d', code)
			return None

		if check_stdout:
			if not stdout_data:
				report_failure(args, stderr_data, 'External process did not write to standard output')
				return None

		if check_path is not None:
			st = try_lstat(check_path)
			if st is None or not st.st_size:
				report_failure(args, stderr_data, 'External process did not create or write to output path: %s', check_path)
				return None

		if check_fd is not None:
			st = try_fstat(check_fd)
			if st is None or not st.st_size:
				report_failure(args, stderr_data, 'External process did not write to file descriptor: %d', check_fd)
				return None

		parse_json: bool = False
		if stdout_data is not None and (encoding is not None or errors is not None):
			if encoding == 'json':
				parse_json = True
				encoding = 'utf-8'

			try:
				assert isinstance(stdout_data, bytes)
				stdout_data = stdout_data.decode(encoding or default_encoding, errors=errors or 'strict')
			except UnicodeDecodeError as e:
				report_failure(args, stderr_data, 'External process output could not be decoded: %r', stdout_data)
				return None

			stdout_data = stdout_data.replace('\r\n', '\n').replace('\r', '\n')

		json_data: typing.Any = None
		if parse_json:
			try:
				json_data = json.loads(stdout_data)
			except json.JSONDecodeError:
				report_failure(args, stderr_data, 'External process did not emit well-formed JSON: %r', stdout_data)
				return None

		return ExternalProcess(args, code, stdout_data, stderr_data, json_data)
