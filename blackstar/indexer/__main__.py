import os
import typing
import logging
import argparse
import pathlib
import signal
import setproctitle

from ..common.logging import MESSAGE_FORMAT_THREAD as MESSAGE_FORMAT, DATE_FORMAT, setup_logging, parse_log_level
from .logging import LOGGER
from .signal import ReloadException, setup_signals, setup_alarm, wait_for_hangup
from .context import Context
from .action import execute_actions
from .task import execute_tasks

def create_parser() -> argparse.ArgumentParser:
	parser = argparse.ArgumentParser('indexer')

	parser.add_argument('-c', '--configuration', type=pathlib.Path, required=True)
	parser.add_argument('-l', '--log-file', type=pathlib.Path, default='-')
	parser.add_argument('-L', '--log-level', type=parse_log_level, default=logging.INFO)
	parser.add_argument('-s', '--service', action='store_true')
	parser.add_argument('-w', '--wait', action='store_true')
	parser.add_argument('-T', '--tick', type=int)
	parser.add_argument('-t', '--threads', type=int, default=len(os.sched_getaffinity(0)) or 1)

	return parser

def main() -> None:
	parser = create_parser()
	options = parser.parse_args()

	setup_logging(options.log_file, options.log_level, MESSAGE_FORMAT, DATE_FORMAT, True)
	setup_signals(options.service, options.tick is not None)
	setproctitle.setproctitle('blackstar-indexer')

	if options.service:
		service(options.configuration, options.wait, options.tick, options.threads)
	else:
		run(options.configuration, options.threads, True)

def service(configuration: pathlib.Path, wait: bool, tick: typing.Optional[int], threads: int) -> None:
	LOGGER.info('Service started')

	iteration = 0
	reload_count = 0
	while True:
		if tick is not None:
			setup_alarm(tick)

		try:
			if reload_count:
				LOGGER.info('Restarting full index on signal (attempt #%u)', reload_count)
			elif iteration:
				LOGGER.info('Starting full index on signal')
			elif not wait:
				LOGGER.info('Starting full index')
			else:
				LOGGER.info('Starting partial index')

			run(configuration, threads, bool(iteration) or not wait)
			iteration += 1
		except ReloadException:
			iteration += 1
			reload_count += 1
			continue
		except Exception as e:
			LOGGER.critical('Error occured during indexing:', exc_info=e)

		LOGGER.info('Index complete')
		reload_count = 0
		wait_for_hangup()

def run(configuration: pathlib.Path, threads: int, full: bool) -> None:
	with Context(configuration, threads) as context:
		if full:
			execute_actions(context)
		context.database_ref.close()
		execute_tasks(context)

if __name__ == '__main__':
	main()
