import os
import typing
import dataclasses
import pathlib

from ....common import thumbnail
from ...logging import LOGGER
from ...context import Context
from ...utility import DirectoryHandle
from ..base import Action

TMethod = typing.TypeVar('TMethod')

class CleanThumbnailAction(Action):
	name = 'clean_thumbnail'

	@dataclasses.dataclass(eq=False)
	class State:
		handle: DirectoryHandle
		display: pathlib.PurePath
		depth: int
		deleted: int = 0

	def __init__(self, context: Context) -> None:
		super().__init__(context)
		self.state: typing.Optional['CleanThumbnailAction.State'] = None

	def execute(self) -> None:
		handle = DirectoryHandle.create(self.context.configuration.directory.thumbnail)
		if handle is None:
			return

		with handle:
			_, deleted = self.recurse(handle, pathlib.PurePath(), 0)

		if deleted:
			LOGGER.info('Deleted %d thumbnail directory entries', deleted)

	def recurse(self, handle: DirectoryHandle, display: pathlib.PurePath, depth: int) -> tuple[bool, int]:
		old_state = self.state
		new_state = self.State(handle, display, depth)

		try:
			self.state = new_state
			result = self.process()
		finally:
			self.state = old_state

		return result, new_state.deleted

	def process(self) -> bool:
		assert self.state is not None

		count = 0
		to_delete: list[str] = []
		for entry in self.state.handle.enumerate():
			self.checkpoint()

			count += 1
			parsed = self.parse(entry)
			if parsed is None:
				LOGGER.warning('Aberrant thumbnail directory entry: %s', self.state.display / entry.name)
				continue

			if self.visit(entry.name, parsed):
				to_delete.append(entry.name)

		deleted = 0
		for name in to_delete:
			if self.remove(name):
				deleted += 1
				self.state.deleted += 1

		return deleted >= count

	def get_method(self, intermediate: TMethod, leaf: TMethod) -> TMethod:
		assert self.state is not None
		if self.state.depth == thumbnail.SPREAD:
			return leaf
		elif self.state.depth in range(0, thumbnail.SPREAD):
			return intermediate
		else:
			raise ValueError('too deep')

	def parse(self, entry: os.DirEntry[str]) -> typing.Optional[tuple[typing.Any, ...]]:
		return self.get_method(self.parse_intermediate, self.parse_leaf)(entry)

	def visit(self, name: str, parsed: tuple[typing.Any, ...]) -> bool:
		return self.get_method(self.visit_intermediate, self.visit_leaf)(name, parsed)

	def remove(self, name: str) -> bool:
		return self.get_method(self.remove_intermediate, self.remove_leaf)(name)

	def parse_intermediate(self, entry: os.DirEntry[str]) -> typing.Optional[tuple[()]]:
		if not entry.is_dir(follow_symlinks=False):
			return None
		if len(entry.name) != 1:
			return None
		return ()

	def visit_intermediate(self, name: str, parsed: tuple[typing.Any, ...]) -> bool:
		assert self.state is not None

		sub_handle = self.state.handle.open(name)
		if sub_handle is None:
			return False

		with sub_handle:
			result, deleted = self.recurse(sub_handle, self.state.display / name, self.state.depth + 1)

		self.state.deleted += deleted
		return result

	def remove_intermediate(self, name: str) -> bool:
		assert self.state is not None
		return self.state.handle.rmdir(name)

	def parse_leaf(self, entry: os.DirEntry[str]) -> typing.Optional[tuple[str, bool]]:
		assert self.state is not None

		if not entry.is_file(follow_symlinks=False):
			return None

		id, extension = os.path.splitext(entry.name)
		if len(id) != thumbnail.ID_LENGTH:
			return None

		extension = extension.lower()
		if extension == thumbnail.EXTENSION:
			animated = False
		elif extension == thumbnail.EXTENSION_ANIMATED:
			animated = True
		else:
			return None

		for i, part in enumerate(self.state.display.parts):
			if id[i] != part:
				return None

		return id, animated

	def visit_leaf(self, name: str, parsed: tuple[typing.Any, ...]) -> bool:
		if parsed[1]:
			stmt = 'SELECT EXISTS(SELECT * FROM metadata WHERE preview_1x = :id OR preview_2x = :id) AS any'
		else:
			stmt = 'SELECT EXISTS(SELECT * FROM metadata WHERE thumbnail_1x = :id OR thumbnail_2x = :id OR thumbnail_3x = :id OR thumbnail_4x = :id) AS any'

		row = self.context.database.execute(stmt, {'id': parsed[0]}).fetchone()
		return not row['any']

	def remove_leaf(self, name: str) -> bool:
		assert self.state is not None
		LOGGER.info('Deleting: %s', name)
		return self.state.handle.unlink(name)
