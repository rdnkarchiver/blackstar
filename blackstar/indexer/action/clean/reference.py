import time

from ...logging import LOGGER
from ..base import Action

class CleanReferenceAction(Action):
	name = 'clean_reference'

	def execute(self) -> None:
		with self.context.database_ref:
			cursor = self.context.database_ref.execute('DELETE FROM reference WHERE ? >= expiration', (int(time.time()),))
			count = cursor.rowcount

		if count:
			LOGGER.info('Deleted %d expired references', count)

		with self.context.database_ref:
			cursor = self.context.database_ref.execute('DELETE FROM reference WHERE path NOT IN (SELECT path FROM bucket.bucket)')
			count = cursor.rowcount

		self.context.database_ref.execute('DETACH DATABASE bucket')

		if count:
			LOGGER.info('Deleted %d invalid references', count)
