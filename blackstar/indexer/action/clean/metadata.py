from ...logging import LOGGER
from ..base import Action

class CleanMetadataAction(Action):
	name = 'clean_metadata'

	def execute(self) -> None:
		self.checkpoint()

		with self.context.database:
			if self.context.device_mapper.root_node is None:
				cursor = self.context.database.execute('DELETE FROM metadata WHERE NOT EXISTS (SELECT * FROM hierarchy WHERE device_child = metadata.device AND inode_child = metadata.inode LIMIT 1)')
			else:
				cursor = self.context.database.execute('DELETE FROM metadata WHERE NOT (device = ? AND inode = ?) AND NOT EXISTS (SELECT * FROM hierarchy WHERE device_child = metadata.device AND inode_child = metadata.inode LIMIT 1)', (self.context.device_mapper.root_node.device, self.context.device_mapper.root_node.inode))
			count = cursor.rowcount

		if count:
			LOGGER.info('Deleted %d metadata entries', count)
