import os
import typing
import dataclasses
import stat
import pathlib

from ...common.node import Node
from ...common.reference import get_or_create_reference_nolock
from ..logging import LOGGER
from ..context import Context
from ..utility import DirectoryHandle
from .base import Action

class ScanAction(Action):
	name = 'scan'

	@dataclasses.dataclass(eq=False, frozen=True)
	class State:
		handle: DirectoryHandle
		path_pure: pathlib.PurePosixPath
		node: Node
		st: os.stat_result

	def __init__(self, context: Context) -> None:
		super().__init__(context)
		self.state: typing.Optional['ScanAction.State'] = None

	def execute(self) -> None:
		self.attach_reference_bucket()

		with self.context.database:
			self.checkpoint()
			self.clear_hierarchy()
			self.checkpoint()
			self.enter_root()
			self.checkpoint()
			self.update_link_count()

	def attach_reference_bucket(self) -> None:
		self.context.database_ref.execute('ATTACH DATABASE \':memory:\' AS bucket')
		self.context.database_ref.execute('CREATE TABLE bucket.bucket (path PATH BLOB NOT NULL UNIQUE)')
		self.context.database_ref.execute('CREATE UNIQUE INDEX bucket.bucket_path ON bucket(path)')

	def clear_hierarchy(self) -> None:
		self.context.database.execute('DELETE FROM hierarchy')

	def enter_root(self) -> None:
		handle = DirectoryHandle.create(self.context.configuration.directory.root)
		if handle is None:
			return

		with handle:
			st = handle.stat()
			if st is None or not stat.S_ISDIR(st.st_mode):
				return

			node = self.context.device_mapper.resolve(st)
			if node is None:
				return

			self.recurse(handle, pathlib.PurePosixPath(), node, st)

	def enter(self, name: str, node: Node, st: os.stat_result) -> bool:
		assert self.state is not None

		sub_handle = self.state.handle.open(name)
		if sub_handle is None:
			return False

		with sub_handle:
			self.recurse(sub_handle, self.state.path_pure / name, node, st)

		return True

	def recurse(self, handle: DirectoryHandle, path_pure: pathlib.PurePosixPath, node: Node, st: os.stat_result) -> None:
		old_state = self.state
		new_state = self.State(handle, path_pure, node, st)

		try:
			self.state = new_state
			self.visit()
		finally:
			self.state = old_state

	def enumerate(self) -> typing.Iterator[tuple[str, Node, os.stat_result]]:
		if self.state is None:
			return

		for entry in self.state.handle.enumerate():
			st = self.state.handle.stat(entry)
			if st is None:
				continue
			node = self.context.device_mapper.resolve(st)
			if node is None:
				continue
			yield entry.name, node, st

	def visit(self) -> None:
		assert self.state is not None

		self.checkpoint()

		LOGGER.info('Scanning: %s', self.state.path_pure)

		if self.insert_metadata(self.state.node, self.state.st, 'D'):
			self.insert_tasks(self.state.node, 'D')

		directories: list[tuple[str, Node, os.stat_result]] = []
		files: list[tuple[str, Node, os.stat_result]] = []
		names: list[str] = []

		for t in self.enumerate():
			if stat.S_ISDIR(t[2].st_mode):
				directories.append(t)
			elif stat.S_ISREG(t[2].st_mode):
				files.append(t)

		directories.sort(key=lambda x: x[0].lower())

		for name, node, st in directories:
			self.checkpoint()
			if not self.enter(name, node, st):
				names.append(name)
				if self.insert_metadata(node, st, 'D'):
					self.insert_tasks(node, 'D')
			self.insert_hierarchy(node, name)

		for name, node, st in files:
			self.checkpoint()
			names.append(name)

			file_path_pure = self.state.path_pure / name
			file_type = self.get_file_type(name, st.st_size)

			if self.insert_metadata(node, st, file_type):
				self.insert_tasks(node, file_type)

			self.insert_hierarchy(node, name)

		with self.context.database_ref:
			self.checkpoint()
			self.update_reference(self.state.path_pure)

			for name in names:
				self.checkpoint()
				self.update_reference(self.state.path_pure / name)

	def insert_metadata(self, node: Node, st: os.stat_result, type: str) -> bool:
		previous = self.context.database.execute('SELECT type, size, atime, mtime, ctime FROM metadata WHERE device = ? AND inode = ?', (node.device, node.inode)).fetchone()

		if previous is not None and (previous['type'] != type or previous['size'] != st.st_size or previous['mtime'] != st.st_mtime):
			LOGGER.info('Deleting stale metadata for node %s', node)
			self.context.database.execute('DELETE FROM metadata WHERE device = ? AND inode = ?', (node.device, node.inode))
			previous = None

		if previous is None:
			self.context.database.execute('INSERT INTO metadata (device, inode, type, size, atime, mtime, ctime) VALUES (?, ?, ?, ?, ?, ?, ?)', (node.device, node.inode, type, st.st_size, st.st_atime, st.st_mtime, st.st_ctime))
			return True

		if previous['atime'] != st.st_atime or previous['ctime'] != st.st_ctime:
			self.context.database.execute('UPDATE metadata SET atime = ?, ctime = ? WHERE device = ? AND inode = ?', (st.st_atime, st.st_ctime, node.device, node.inode))

		return False

	def insert_tasks(self, node: Node, type: str) -> None:
		if type != 'D':
			self.insert_task(node, 'hash')
		if type in ('I', 'A', 'V'):
			self.insert_task(node, 'basic')
		if type in ('I', 'V'):
			self.insert_task(node, 'exif')
			self.insert_task(node, 'perceptual')
			self.insert_task(node, 'thumbnail_1x')
			self.insert_task(node, 'thumbnail_2x')
			self.insert_task(node, 'thumbnail_3x')
			self.insert_task(node, 'thumbnail_4x')
		if type == 'V':
			self.insert_task(node, 'preview_1x')
			self.insert_task(node, 'preview_2x')
		if type == 'I':
			self.insert_task(node, 'similar_i')
		elif type == 'V':
			self.insert_task(node, 'similar_v')

	def insert_task(self, node: Node, task: str) -> None:
		self.context.database.execute('INSERT INTO queue (device, inode, task) VALUES (?, ?, ?)', (node.device, node.inode, task))

	def insert_hierarchy(self, node: Node, name: str) -> None:
		assert self.state is not None

		parent_node = self.state.node
		child_node = node
		true_name = pathlib.PurePosixPath(name)

		self.context.database.execute('INSERT INTO hierarchy (device_parent, inode_parent, device_child, inode_child, name) VALUES (?, ?, ?, ?, ?)', (parent_node.device, parent_node.inode, child_node.device, child_node.inode, true_name))

	def get_file_type(self, name: str, size: int) -> str:
		if not size:
			return 'F'

		extension = os.path.splitext(name)[1].lower()
		if extension in self.context.configuration.extension.image:
			return 'I'
		elif extension in self.context.configuration.extension.audio:
			return 'A'
		elif extension in self.context.configuration.extension.video:
			return 'V'
		else:
			return 'F'

	def update_reference(self, path: pathlib.PurePosixPath) -> None:
		self.context.database_ref.execute('INSERT OR IGNORE INTO bucket.bucket (path) VALUES (?)', (path,))
		get_or_create_reference_nolock(self.context.database_ref, None, None, path, False)
		get_or_create_reference_nolock(self.context.database_ref, None, None, path, True)

	def update_link_count(self) -> None:
		self.context.database.execute('UPDATE metadata SET count_link = (SELECT COUNT(*) - 1 FROM hierarchy WHERE hierarchy.device_child = metadata.device AND hierarchy.inode_child = metadata.inode)')
		if self.context.device_mapper.root_node is not None:
			self.context.database.execute('UPDATE metadata SET count_link = 0 WHERE device = ? AND inode = ?', (self.context.device_mapper.root_node.device, self.context.device_mapper.root_node.inode))
