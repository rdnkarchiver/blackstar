import abc

from ..context import Context

class Action(abc.ABC):
	name: str = 'action'

	def __init__(self, context: Context) -> None:
		self.context = context

	@abc.abstractmethod
	def execute(self) -> None:
		pass

	def checkpoint(self) -> None:
		pass
