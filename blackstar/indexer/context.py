import os
import types
import typing
import contextlib
import sqlite3
import multiprocessing.pool
import multiprocessing.dummy

from ..common.configuration import Configuration, load as load_configuration
from ..common.device import DeviceMapper
from ..common.database import open_media_database, open_reference_database

from .logging import LOGGER

class Context(contextlib.AbstractContextManager['Context']):
	def __init__(self, path: typing.Union[str, os.PathLike[str]], threads: int) -> None:
		self.configuration = load_configuration(path)
		self.device_mapper = DeviceMapper(self.configuration)
		self.database = open_media_database(self.configuration, True)
		self.database_ref = open_reference_database(self.configuration, True)

		self.pool: typing.Optional[multiprocessing.pool.Pool] = None
		if threads:
			self.pool = multiprocessing.dummy.Pool(threads)

	def __enter__(self) -> 'Context':
		return self

	def __exit__(self, exc_type: typing.Optional[typing.Type[BaseException]], exc_value: typing.Optional[BaseException], traceback: typing.Optional[types.TracebackType]) -> None:
		self.close()

	def close(self) -> None:
		if self.pool is not None:
			self.pool.terminate()
			self.pool.join()

		self.database_ref.close()
		self.database.close()
