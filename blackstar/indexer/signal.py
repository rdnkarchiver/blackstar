import sys
import typing
import types
import signal

from .logging import LOGGER

pending: dict[str, signal.Signals] = {}

class ReloadException(Exception):
	pass

def setup_signals(hup: bool, alarm: bool) -> None:
	signal.signal(signal.SIGINT, exit_handler)
	signal.signal(signal.SIGTERM, exit_handler)
	if hup:
		signal.signal(signal.SIGHUP, reload_handler)
	if alarm:
		signal.signal(signal.SIGALRM, reload_handler)

def setup_alarm(tick: int) -> None:
	signal.alarm(tick)

def exit_handler(signum: int, frame: typing.Optional[types.FrameType]) -> None:
	pending['exit'] = signal.Signals(signum)

def reload_handler(signum: int, frame: typing.Optional[types.FrameType]) -> None:
	pending['reload'] = signal.Signals(signum)

def checkpoint() -> None:
	received: typing.Optional[signal.Signals] = pending.pop('exit', None)
	if received:
		LOGGER.info('Received %s, exiting', received.name)
		sys.exit()

	received = pending.pop('reload', None)
	if received:
		LOGGER.info('Received %s, reloading', received.name)
		raise ReloadException()

def wait_for_hangup() -> None:
	try:
		while True:
			signal.pause()
			checkpoint()
	except ReloadException:
		pass
