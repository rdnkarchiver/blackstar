#include <stdint.h>
#include <stddef.h>

void hamming_array_u64_fast(const uint64_t* input, uint64_t* output, uint64_t value, size_t count)
{
	for (const uint64_t* end = input + count; input < end; ++input, ++output)
		*output = __builtin_popcountll(*input ^ value);
}
