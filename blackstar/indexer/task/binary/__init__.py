import os

THIS_PATH = os.path.realpath(os.path.dirname(__file__))

def get_binary_path(name: str) -> str:
	return os.path.join(THIS_PATH, name)
