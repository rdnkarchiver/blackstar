import typing

from ..context import Context
from .base import Task
from .executor import execute_tasks as execute_tasks_ex, execute_task
from .basic import BasicTask
from .exif import ExifTask
from .thumbnail import Thumbnail1xTask, Thumbnail2xTask, Thumbnail3xTask, Thumbnail4xTask
from .preview import Preview1xTask, Preview2xTask
from .perceptual import PerceptualTask
from .similar import SimilarImageTask, SimilarVideoTask
from .hash import HashTask

TASKS: list[typing.Type[Task]] = [
	BasicTask,
	ExifTask,
	Thumbnail2xTask,
	Thumbnail1xTask,
	Preview2xTask,
	Preview1xTask,
	Thumbnail4xTask,
	Thumbnail3xTask,
	PerceptualTask,
	SimilarImageTask,
	SimilarVideoTask,
	HashTask
]

def execute_tasks(context: Context) -> None:
	return execute_tasks_ex(context, TASKS)
