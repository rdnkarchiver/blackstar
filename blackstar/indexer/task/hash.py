import os
import typing
import stat
import mmap
import hashlib

from ...common.node import Node
from ..logging import LOGGER
from ..context import Context
from ..utility import try_open_file
from .path import PathTask

class HashTask(PathTask):
	name = 'hash'
	description = 'Hashing'
	chunk = 32 * 1024 * 1024

	def __init__(self, context: Context, rowid: int, node: Node) -> None:
		super().__init__(context, rowid, node)
		self.hash_md5: typing.Optional[bytes] = None
		self.hash_sha1: typing.Optional[bytes] = None
		self.hash_sha256: typing.Optional[bytes] = None

	def execute(self) -> None:
		fd = self.get_fd()
		if not fd:
			return

		try:
			self.execute_fd(fd)
		finally:
			os.close(fd)

	def execute_fd(self, fd: int) -> None:
		st = os.fstat(fd)
		if not stat.S_ISREG(st.st_mode):
			return

		if not st.st_size:
			self.execute_view(memoryview(b''))
			return

		with mmap.mmap(fd, st.st_size, access=mmap.ACCESS_READ) as m:
			with memoryview(m) as view:
				self.execute_view(view)

	def execute_view(self, view: memoryview) -> None:
		hasher_md5 = hashlib.md5()
		hasher_sha1 = hashlib.sha1()
		hasher_sha256 = hashlib.sha256()

		offset = 0
		remaining = len(view)
		while remaining:
			self.checkpoint()

			size = min(remaining, self.chunk)

			with view[offset:offset+size] as subview:
				hasher_md5.update(subview)
				hasher_sha1.update(subview)
				hasher_sha256.update(subview)

			offset += size
			remaining -= size

		self.hash_md5 = hasher_md5.digest()
		self.hash_sha1 = hasher_sha1.digest()
		self.hash_sha256 = hasher_sha256.digest()

	def update_locked(self) -> None:
		self.context.database.execute('UPDATE metadata SET hash_md5 = ?, hash_sha1 = ?, hash_sha256 = ? WHERE device = ? AND inode = ?', (self.hash_md5, self.hash_sha1, self.hash_sha256, self.node.device, self.node.inode))
		super().update_locked()

	@classmethod
	def finalize(cls, context: Context) -> None:
		with context.database:
			context.database.execute('UPDATE metadata SET count_duplicate = (SELECT COUNT(*) FROM metadata AS metadata_sub WHERE metadata_sub.hash_sha256 = metadata.hash_sha256) - 1 WHERE hash_sha256 IS NOT NULL')
