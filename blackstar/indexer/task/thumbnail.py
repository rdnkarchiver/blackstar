import os
import typing
import subprocess

from ...common.node import Node
from ...common.thumbnail import create_new
from ..context import Context
from ..external import try_execute
from .ffmpeg import FFMpegTask

class Thumbnail1xTask(FFMpegTask):
	name = 'thumbnail_1x'
	description = 'Rendering thumbnail @1x'
	size = 128
	column = name

	def __init__(self, context: Context, rowid: int, node: Node) -> None:
		super().__init__(context, rowid, node)
		self.thumbnail: typing.Optional[str] = None

	def execute(self) -> None:
		path = self.get_path()
		if path is None:
			return

		thumbnail, fd, thumbnail_path = create_new(self.context.configuration, False)
		os.close(fd)

		args = self.ffmpeg()

		if self.seek is not None:
			args.extend(['-ss', str(round(self.seek, 3))])

		args.extend([
			'-an', '-sn', '-dn',
			'-i', os.fspath(path),
			'-map_metadata', '-1',
			'-filter:v', 'scale={0}:{0}:force_original_aspect_ratio=decrease,setsar=1,setdar=a'.format(self.size),
			'-frames:v', '1',
			'-codec:v', 'mjpeg',
			'-y', '--', os.fspath(thumbnail_path)
		])

		process = try_execute(args, stdout=subprocess.DEVNULL, check_path=thumbnail_path)
		if process is None:
			os.unlink(thumbnail_path)
			return

		self.thumbnail = thumbnail

	def update_locked(self) -> None:
		self.context.database.execute('UPDATE metadata SET {} = ? WHERE device = ? AND inode = ?'.format(self.column), (self.thumbnail, self.node.device, self.node.inode))
		super().update_locked()

class Thumbnail2xTask(Thumbnail1xTask):
	name = 'thumbnail_2x'
	description = 'Rendering thumbnail @2x'
	size = Thumbnail1xTask.size * 2
	column = name

class Thumbnail3xTask(Thumbnail1xTask):
	name = 'thumbnail_3x'
	description = 'Rendering thumbnail @3x'
	size = Thumbnail1xTask.size * 3
	column = name

class Thumbnail4xTask(Thumbnail1xTask):
	name = 'thumbnail_4x'
	description = 'Rendering thumbnail @4x'
	size = Thumbnail1xTask.size * 4
	column = name
