import os
import typing
import subprocess

from ...common.node import Node
from ..context import Context
from ..utility import try_convert
from ..external import try_execute
from .path import PathTask
from .ffmpeg import FFMpegTask

class BasicTask(PathTask):
	name = 'basic'
	description = 'Extracting basic metadata'

	def __init__(self, context: Context, rowid: int, node: Node) -> None:
		super().__init__(context, rowid, node)
		self.width: typing.Optional[float] = None
		self.height: typing.Optional[float] = None
		self.duration: typing.Optional[float] = None
		self.vcodec: typing.Optional[str] = None
		self.acodec: typing.Optional[str] = None

	def prepare(self) -> None:
		super().prepare()
		self.paths = FFMpegTask.filter(self.paths)

	def execute(self) -> None:
		path = self.get_path()
		if path is None:
			return

		args = [
			os.fspath(self.context.configuration.binary.ffprobe),
			'-v', 'quiet',
			'-print_format', 'json',
			'-show_entries',
			'format=duration:stream=codec_type,codec_name,width,height',
			'-i',
			os.fspath(path)
		]

		process = try_execute(args, stdout=subprocess.PIPE, encoding='json', check_stdout=True)
		if process is None or not isinstance(process.json, dict):
			return

		empty: dict[str, typing.Any] = {}
		info: dict[str, typing.Any] = process.json
		video_stream: dict[str, typing.Any] = empty
		audio_stream: dict[str, typing.Any] = empty

		for stream in info.get('streams', empty):
			codec_type = stream.get('codec_type')
			if codec_type == 'video' and video_stream is empty:
				video_stream = stream
			elif codec_type == 'audio' and audio_stream is empty:
				audio_stream = stream

		self.width = try_convert(int, video_stream.get('width'))
		self.height = try_convert(int, video_stream.get('height'))
		self.duration = try_convert(float, info.get('format', empty).get('duration'))
		self.vcodec = try_convert(str, video_stream.get('codec_name'))
		self.acodec = try_convert(str, audio_stream.get('codec_name'))

	def update_locked(self) -> None:
		self.context.database.execute('UPDATE metadata SET width = ?, height = ?, duration = ?, vcodec = ?, acodec = ? WHERE device = ? AND inode = ?', (self.width, self.height, self.duration, self.vcodec, self.acodec, self.node.device, self.node.inode))
		super().update_locked()
