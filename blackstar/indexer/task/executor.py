import typing
import queue

from ...common.node import Node
from ..logging import LOGGER
from ..signal import ReloadException, checkpoint
from ..context import Context
from .base import Task

class TaskAbortException(Exception):
	pass

def execute_tasks(context: Context, task_types: list[typing.Type[Task]]) -> None:
	for task_type in task_types:
		checkpoint()
		execute_task(context, task_type)

def execute_task(context: Context, task_type: typing.Type[Task]) -> None:
	LOGGER.info('Executing tasks: %s', task_type.name)

	task_type.initialize(context)
	try:
		tasks = create_tasks(context, task_type)
		if task_type.batch or context.pool is None:
			execute_sequential(context, tasks)
		else:
			execute_parallel(context, tasks)
	except (ReloadException, SystemExit):
		task_type.finalize(context)
		raise
	else:
		task_type.finalize(context)

def create_tasks(context: Context, task_type: typing.Type[Task]) -> typing.Generator[Task, None, None]:
	cursor = context.database.execute('SELECT ROWID, device, inode FROM queue WHERE task = ? ORDER BY device, inode', (task_type.name,))
	cursor.arraysize = 256

	if not task_type.batch:
		for row in cursor:
			yield task_type(context, row['ROWID'], Node(row['device'], row['inode'])) # type: ignore
		return

	rows = cursor.fetchall()
	del cursor

	if not rows:
		return

	rowids = [row['ROWID'] for row in rows]
	nodes = [Node(row['device'], row['inode']) for row in rows]
	del rows

	yield task_type(context, rowids, nodes) # type: ignore

def execute_sequential(context: Context, tasks: typing.Generator[Task, None, None]) -> None:
	for task in tasks:
		checkpoint()
		task.checkpoint = checkpoint # type: ignore[assignment]
		task.prepare()
		task.announce()
		task.execute()
		task.update()

def execute_parallel(context: Context, tasks: typing.Generator[Task, None, None]) -> None:
	pending: int = 0
	results: queue.SimpleQueue[typing.Union[Task, BaseException]] = queue.SimpleQueue()
	abort: bool = False

	assert context.pool is not None

	def task_wrapper(task: Task) -> Task:
		task.checkpoint()
		task.announce()
		task.execute()
		return task

	def task_checkpoint() -> None:
		if abort:
			raise TaskAbortException()

	def drain(block: bool, finalize: bool) -> None:
		nonlocal pending
		while pending:
			try:
				result = results.get(block)
			except queue.Empty:
				break

			pending -= 1

			if isinstance(result, Task):
				result.update()
			elif isinstance(result, TaskAbortException):
				pass
			elif isinstance(result, BaseException):
				if finalize:
					LOGGER.critical('Unhandled exception occured:', exc_info=result)
				else:
					raise result

			if not finalize:
				checkpoint()

	try:
		for task in tasks:
			checkpoint()

			task.checkpoint = task_checkpoint # type: ignore[assignment]
			task.prepare()

			context.pool.apply_async(task_wrapper, (task,), {}, results.put, results.put)
			pending += 1

			drain(False, False)

		drain(True, False)
	except:
		tasks.close()
		abort = True
		raise
	finally:
		drain(True, True)
