import os
import sys
import typing
import types
import pathlib
import abc
import contextlib

from .logging import LOGGER

def report_failure(e: Exception, call: str, *args: typing.Any) -> None:
	call_str = '{}({})'.format(call, ', '.join(repr(os.fspath(x)) if isinstance(x, os.PathLike) else repr(x) for x in args))
	if isinstance(e, OSError):
		LOGGER.error('%s: %s', call_str, e.strerror)
	else:
		LOGGER.error('%s:', call_str, exc_info=e)

def try_scandir(directory: typing.Union[int, str, os.PathLike[str]]) -> typing.Iterator[os.DirEntry[str]]:
	try:
		return os.scandir(directory)
	except Exception as e:
		report_failure(e, 'scandir', directory)
		return iter(())

def try_lstat(path: typing.Union[str, os.PathLike[str]], dirfd: typing.Optional[int] = None) -> typing.Optional[os.stat_result]:
	try:
		return os.lstat(path, dir_fd=dirfd)
	except Exception as e:
		if dirfd is None:
			report_failure(e, 'lstat', path)
		else:
			report_failure(e, 'fstatat', dirfd, path)
		return None

def try_fstat(fd: int) -> typing.Optional[os.stat_result]:
	try:
		return os.fstat(fd)
	except Exception as e:
		report_failure(e, 'fstat', fd)
		return None

def try_direntry_lstat(entry: os.DirEntry[str]) -> typing.Optional[os.stat_result]:
	try:
		return entry.stat(follow_symlinks=False)
	except Exception as e:
		report_failure(e, 'DirEntry.stat', entry.name)
		return None

def try_open(path: typing.Union[str, os.PathLike[str]], flags: int, dirfd: typing.Optional[int] = None) -> typing.Optional[int]:
	try:
		return os.open(path, flags, dir_fd=dirfd)
	except Exception as e:
		if dirfd is None:
			report_failure(e, 'open', path)
		else:
			report_failure(e, 'openat', dirfd, path)
		return None

def try_open_file(path: typing.Union[str, os.PathLike[str]], dirfd: typing.Optional[int] = None) -> typing.Optional[int]:
	return try_open(path, os.O_RDONLY|os.O_NOFOLLOW|os.O_CLOEXEC, dirfd)

def try_open_directory(path: typing.Union[str, os.PathLike[str]], dirfd: typing.Optional[int] = None) -> typing.Optional[int]:
	return try_open(path, os.O_RDONLY|os.O_NOFOLLOW|os.O_DIRECTORY|os.O_CLOEXEC, dirfd)

def try_unlink(path: typing.Union[str, os.PathLike[str]], dirfd: typing.Optional[int] = None) -> bool:
	try:
		os.unlink(path, dir_fd=dirfd)
		return True
	except Exception as e:
		if dirfd is None:
			report_failure(e, 'unlink', path)
		else:
			report_failure(e, 'unlinkat', dirfd, path)
		return False

def try_rmdir(path: typing.Union[str, os.PathLike[str]], dirfd: typing.Optional[int] = None) -> bool:
	try:
		os.rmdir(path, dir_fd=dirfd)
		return True
	except Exception as e:
		if dirfd is None:
			report_failure(e, 'rmdir', path)
		else:
			report_failure(e, 'unlinkat', dirfd, path)
		return False

T = typing.TypeVar('T')

def try_convert(type: typing.Type[T], value: typing.Any) -> typing.Optional[T]:
	if value is None:
		return None
	if isinstance(value, type):
		return value
	try:
		return type(value) # type: ignore
	except ValueError:
		return None

class DirectoryHandle(contextlib.AbstractContextManager['DirectoryHandle']):
	@abc.abstractmethod
	def enumerate(self) -> typing.Iterator[os.DirEntry[str]]:
		pass

	@abc.abstractmethod
	def stat(self, name: typing.Union[str, os.DirEntry[str], None] = None) -> typing.Optional[os.stat_result]:
		pass

	@abc.abstractmethod
	def open(self, name: str) -> typing.Optional['DirectoryHandle']:
		pass

	@abc.abstractmethod
	def unlink(self, name: str) -> bool:
		pass

	@abc.abstractmethod
	def rmdir(self, name: str) -> bool:
		pass

	def close(self) -> None:
		pass

	def __enter__(self) -> 'DirectoryHandle':
		return self

	def __exit__(self, exc_type: typing.Optional[typing.Type[BaseException]], exc_value: typing.Optional[BaseException], traceback: typing.Optional[types.TracebackType]) -> None:
		self.close()

	@staticmethod
	def create(path: pathlib.Path) -> typing.Optional['DirectoryHandle']:
		if os.supports_dir_fd:
			return DescriptorDirectoryHandle.create(path)
		else:
			return PathDirectoryHandle.create(path)

class PathDirectoryHandle(DirectoryHandle):
	def __init__(self, path: pathlib.Path) -> None:
		self.path = path

	def enumerate(self) -> typing.Iterator[os.DirEntry[str]]:
		return try_scandir(self.path)

	if sys.platform == 'win32':
		def stat(self, name: typing.Union[str, os.DirEntry[str], None] = None) -> typing.Optional[os.stat_result]:
			if isinstance(name, str):
				return try_lstat(self.path / name)
			elif isinstance(name, os.DirEntry):
				return try_lstat(self.path / name.name)
			else:
				return try_lstat(self.path)
	else:
		def stat(self, name: typing.Union[str, os.DirEntry[str], None] = None) -> typing.Optional[os.stat_result]:
			if isinstance(name, str):
				return try_lstat(self.path / name)
			elif isinstance(name, os.DirEntry):
				return try_direntry_lstat(name)
			else:
				return try_lstat(self.path)

	def open(self, name: str) -> 'PathDirectoryHandle':
		return PathDirectoryHandle(self.path / name)

	def unlink(self, name: str) -> bool:
		return try_unlink(self.path / name)

	def rmdir(self, name: str) -> bool:
		return try_rmdir(self.path / name)

	@classmethod
	def create(cls, path: pathlib.Path) -> 'PathDirectoryHandle':
		return cls(path)

class DescriptorDirectoryHandle(DirectoryHandle):
	def __init__(self, fd: int) -> None:
		self.fd = fd

	def enumerate(self) -> typing.Iterator[os.DirEntry[str]]:
		return try_scandir(self.fd)

	if sys.platform == 'win32':
		def stat(self, name: typing.Union[str, os.DirEntry[str], None] = None) -> typing.Optional[os.stat_result]:
			if isinstance(name, str):
				return try_lstat(name, self.fd)
			elif isinstance(name, os.DirEntry):
				return try_lstat(name.name, self.fd)
			else:
				return try_fstat(self.fd)
	else:
		def stat(self, name: typing.Union[str, os.DirEntry[str], None] = None) -> typing.Optional[os.stat_result]:
			if isinstance(name, str):
				return try_lstat(name, self.fd)
			elif isinstance(name, os.DirEntry):
				return try_direntry_lstat(name)
			else:
				return try_fstat(self.fd)

	def open(self, name: str) -> typing.Optional['DescriptorDirectoryHandle']:
		fd = try_open_directory(name, self.fd)
		if fd is None:
			return None
		return DescriptorDirectoryHandle(fd)

	def unlink(self, name: str) -> bool:
		return try_unlink(name, self.fd)

	def rmdir(self, name: str) -> bool:
		return try_rmdir(name, self.fd)

	def close(self) -> None:
		if self.fd < 0:
			return
		os.close(self.fd)
		self.fd = -1

	@classmethod
	def create(cls, path: pathlib.Path) -> typing.Optional['DescriptorDirectoryHandle']:
		fd = try_open_directory(path)
		if fd is None:
			return None
		return DescriptorDirectoryHandle(fd)
