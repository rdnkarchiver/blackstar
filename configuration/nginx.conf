load_module /usr/lib/nginx/modules/ngx_http_headers_more_filter_module.so;
load_module /usr/lib/nginx/modules/ngx_http_geoip2_module.so;

worker_processes 1;

events {}

http {
	include mime.types;
	default_type application/octet-stream;

	sendfile on;

	# https://github.com/P3TERX/GeoLite.mmdb/releases
	geoip2 GeoLite2-Country.mmdb { $geoip2_country_code default=XX country iso_code; }

	server {
		listen 80;
		listen [::]:80;
		server_name blackstar.local;

		more_set_headers "Referrer-Policy: same-origin";
		#more_set_headers "Strict-Transport-Security: max-age=15552000";
		more_set_headers "Content-Security-Policy: default-src 'self'; img-src 'self' www.gravatar.com a.tile.openstreetmap.org b.tile.openstreetmap.org c.tile.openstreetmap.org";
		more_set_headers "X-Frame-Options: deny";
		more_set_headers "X-Content-Type-Options: nosniff";

		location / {
			proxy_pass http://unix:/run/blackstar/browser.socket;
			proxy_http_version 1.1;
			proxy_redirect off;
			proxy_set_header Host $http_host;
			proxy_set_header Connection close;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_set_header X-Forwarded-Proto $scheme;
			proxy_set_header X-Real-IP $remote_addr;
			proxy_set_header CF-IPCountry $geoip2_country_code;
		}

		location = /favicon.ico {
			add_header Cache-Control "public, max-age=3600, s-maxage=86400";
			return 303 /favicon.svg;
		}

		location = /favicon.svg {
			add_header Cache-Control "public, max-age=3600, s-maxage=86400";
			alias /opt/blackstar/blackstar/browser/content/favicon.svg;
		}

		location = /robots.txt {
			add_header Cache-Control "public, max-age=3600, s-maxage=86400";
			alias /opt/blackstar/blackstar/browser/content/robots.txt;
		}

		location /static/ {
			add_header Cache-Control "public, max-age=3600, s-maxage=86400";
			alias /opt/blackstar/blackstar/browser/content/static/;
		}

		location /thumbnail/ {
			add_header Cache-Control "public, immutable, no-transform, max-age=2592000";
			alias /var/blackstar/thumbnail/;
		}

		location /download/ {
			internal;
			alias /media/;
		}
	}
}
