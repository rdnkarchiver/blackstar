# blackstar
blackstar is a media file browser / indexer / duplicate detector.

## installation
all installation instructions and default service files assume an installation location of `/opt/blackstar` and media files to be located in `/media`. adjust paths accordingly.

### dependencies
install the these system packages:
- python 3.10
- nginx
- nginx headers more module
- nginx geoip2 module (optional)
- ffmpeg (+ffprobe)
- exiftool

### extraction
clone the repository:
```shell
cd /opt
git clone https://gitlab.com/darkcypher/blackstar.git
```

or download and extract source:
``` shell
cd /opt
curl -s https://gitlab.com/darkcypher/blackstar/-/archive/master/blackstar-master.tar.gz | tar -xvzf -
mv blackstar-master blackstar
```

### system
create user and group:
```shell
useradd -r -d / -s /sbin/nologin blackstar
```

create runtime directories and files:
```shell
mkdir -p /var/blackstar/{database,thumbnail}
chown -R blackstar:blackstar /var/blackstar
chmod -R o-rwx /var/blackstar
setfacl -n -m u:http:rx,m::rwx /var/blackstar /var/blackstar/thumbnail
```

### services
install and enable systemd services:
```shell
cp /opt/blackstar/systemd/* /etc/systemd/system
systemctl daemon-reload
systemctl enable blackstar-browser.service
systemctl enable blackstar-indexer.service
systemctl enable blackstar-trigger.timer
```

### python
configure the python environment:
```shell
cd /opt/blackstar
python -m venv .venv
source .venv/bin/activate
python -m pip install --upgrade pip
pip install -r requirements.txt
```

### native
build the native library for extra speed (optional):
```shell
cd /opt/blackstar/blackstar/indexer/task/binary
make
```

### nginx
use the `configuration/nginx.conf` file as a base to setup nginx.

if reverse-proxying from a sub-path, add the appropriate `--env SCRIPT_PATH=/path` to the browser arguments.

the geoip support is not strictly necessary, but the header should always be set.

### configuration
the `configuration/blackstar.conf` file holds all the settings.

#### `directory` section
`root` controls the root directory that will be indexed and exposed. it must not be changed afterwards.

`database` and `thumbnail` controls where the database and thumbnails are stored.

the root and thumbnail directories must be readable by nginx as well, and if changed, the nginx configuration must be updated.

#### `device` section
necessary only if the root directory has sub mount points. they must be given stable non-zero numeric values for the database (paths relative to root):

```
"subdirectory"=1
"subdirectory/submount"=2
```

#### `binary` section
paths to the various binaries needed for indexing. if not specified, will try to find from `PATH`.

note: on arch linux, `exiftool` is at `/usr/bin/vendor_perl/exiftool` which is not in the default service `PATH` and must always be specified

#### `extension` section
`image`, `audio`, `video` controls which extensions are recognized as such.

`linked` is the name of the image to use for folder thumbnails.

#### `authentication` section
`parameter` and `cookie` control the name of the query parameter and cookie for the authentication token.

`secret` is used to generate the token securely. **please change this.**

`expiration` is the default token expiration time.

#### `download` section
the nginx internal path prefix used to trigger a file download can be changed here, then the nginx configuration must be updated.

#### `hidden` section
`directory` and `file` control which directories and files are considered hidden. dotfiles are always considered hidden.

`linked` controls if the image used for folder thumbnails is super-hidden.

#### `user.xxx` sections
each of these sections describe a user, with `xxx` being the username.

`password` is the `crypt` version of the password. can be generated from the shell:

```shell
python -c 'import crypt; print(crypt.crypt("password"))'
```

`sequence` should be incremented every time the user information is changed, used to invalidate old tokens.

`email` is the email address (optional), only used for showing a gravatar.

`avatar` is the url to an avatar image (optional), overrides email.

`root` allows to restrict the user to a sub-directory of the root directory.

`can_browse` allows browsing directories, enabled by default

`can_download` allows downloading/viewing of files, enabled by default

`can_share` allows using the share function to create anonymous links, disabled by default

`can_xray` allows seeing hidden files and directories, disabled by default

`can_info` allows accessing the information pages (node/database/location) and viewing the duplicate counts in the directory view

note: giving information rights to a user that has a restricted view will still leak information about the entire directory tree, use carefully.

## usage
the browser is a web application that allows browsing the directory tree. it will always an show up-to-date view of the directories and files, and supplement with metadata from the database. each directory/file is represented by a unique "reference" identifier that never changes for that path.

it's possible to "share" a directory or file with restrictions (specific expiration and force/allow/restrict download) which will create temporary "anonymous" reference identifiers that allow accessing that directory tree / file until it expires.

the indexer is always running, but will only start indexing when receiving a `SIGHUP` (via `systemctl reload blackstar-indexer`). the trigger timer will trigger it once per day, defaulting at 8am.

the indexing process has two phases: actions and tasks. actions will always execute fully and consist of directory scan, database and thumbnail cleanup. tasks are kept in a queue per-file and the execution is fully incremental: a new indexing run will resume executing tasks where it stopped. task types are ordered to try to have the most useful metadata available fast and keep the least useful / slowest ones for last.

all files are keyed in the database by device + inode. device identifiers are synthetic with 0 being root and others being specified in the `device` section of the configuration. real inode numbers are used, so the underlying file system must have stable inode numbers.

## cloudflare
if hosting behind cloudflare, add a page rule for `/*` with **origin cache control** enabled and **cache level** set to **cache everything**.
also set **browser cache ttl** to **respect existing headers**.

the right headers are always sent to enable aggressive caching as much as possible.

enable **ip geolocation** to send the country header, then remove the geoip configuration from nginx.

## customization
all colors, fonts, sizes can be customized in `blackstar/browser/content`.
the accent color (default to purple) must also be changed in a few other places, see the appropriate comment.

to update everything, rebuild the static files:
```shell
cd /opt/blackstar/blackstar/browser/content
make
```

this requires `sassc` to be installed.
